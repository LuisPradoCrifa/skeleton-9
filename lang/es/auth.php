<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'No existe un usuario registrado con esas credenciales.',
    'throttle' => 'Existieron demasiados intentos de ingreso. Por favor intente de nuevo en :seconds segundos.',
    'disabled_user' => 'El usuario se encuentra inhabilitado.',
    'has_no_role' => 'El usuario no tiene un rol asociado.',
    'disabled_role' => 'El rol asociado al usuario se encuentra inhabilitado.',
    'user_inactive' => 'El usuario se encuentra inactivo.',
    'provider_inactive' => 'Proveedor inactivo, comun&iacute;quese con el administrador.',
    'provider_blocked' => 'Proveedor bloqueado, use la opción DESBLOQUEAR CUENTA para activarla.',
    'provider_pending' => 'Este proveedor esta pendiente de aprobaci&oacute;n por un administrador de OSCUS. Recibirá un correo electrónico cuando se le haya habilitado el acceso al sistema',
    'provider_disqualification' => 'Este proveedor se encuentra descalificado por un administrador de OSCUS. Recibirá un correo electrónico cuando se le haya habilitado el acceso al sistema',
    'provider_not_blocked' => 'Su cuenta no se encuentra bloqueada.',
    'provider_on_pending' => 'Su cuenta se encuentra en estado pendiente de verificación.',
    'provider_attempts_fail' => 'Su cuenta ha sido bloqueada por 3 intentos fallidos.',
    'user_has_no_roles' => 'No existen perfiles activos para este usuario.',
    'backoffice' => 'Ingreso Administrador',
    'provider' => 'Ingreso Proveedor',
    'forget_password' => '¿Olvidó su contraseña?',
    'unblock_account' => 'Desbloquear cuenta',
    'send_email' => 'Reestablecer Contraseña',
    'will_send_email' => 'Se enviará un correo electrónico para reestablecer su contraseña',
    'provider_will_send_email' => 'Esta opción le permite restablecer la contraseña mediante el envío de un correo electrónico al correo de contacto registrado.',
    'email_not_exists' => 'El correo electrónico no se encuentra registrado',
    'ruc_not_exists' => 'El ruc o pasaporte no se encuentra registrado',
    'email_sent' => 'Se ha enviado un correo electr&oacute;nico a <strong>:email</strong> para completar el proceso.',
    'inactive_account' => 'Su cuenta está inactiva. Contáctese con el administrador del sistema.',
    'inactive_account_provider' => 'Su cuenta no está activa.',
    'error' => 'Hubo un error al registrar su solicitud. Por favor inténtelo mas tarde.',
    'success' => 'Su contraseña fue actualizada satisfactoriamente.',
    'success_and_active' => 'Su contraseña fue actualizada satisfactoriamente y su cuenta fue activada.',
    'invalid_token' => 'Lo sentimos, el enlace que utiliz&oacute; para restaurar su contrase&ntilde;a no es v&aacute;lido.',
    'update_password' => 'Reestablecer contraseña',
    'update' => 'Actualizar contraseña',
    'time_token_expired' => 'Lo sentimos, el tiempo para la recuperación de su contraseña ha expirado.'
];
