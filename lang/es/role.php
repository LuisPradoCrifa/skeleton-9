<?php
// O
return [
    'title' => 'Roles',
    'labels' => [
        'create' => 'Crear rol',
        'new' => 'Nueva rol',
        'update' => 'Actualizar rol',
        'edit' => 'Editar rol',
        'permissions' => 'Editar permisos',
        'enabled' => 'Activar/Desactivar rol',
        'show' => 'Ver rol',
        'delete' => 'Eliminar rol',
        'index' => 'Lista de roles',
        'name' => 'Nombre',
        'description' => 'Descripción',
        'status' => 'Estado'
    ],
    'placeholders' => [
        'name' => 'Nombre',
        'description' => 'Descripción'
    ],
    'messages' => [
        'success' => [
            'created' => 'Rol creado exitosamente',
            'updated' => 'Rol editado exitosamente',
            'deleted' => 'Rol eliminado exitosamente'
        ],
        'errors' => [
            'create' => 'Ha ocurrido un error al intentar crear el rol',
            'update' => 'Ha ocurrido un error al intentar editar el rol'
        ],
        'exceptions' => [
            'not_found' => 'El rol no existe o no está disponible'
        ],
        'confirm' => [
            'status_on' => '¿Está seguro que desea habilitar el rol seleccionado?',
            'status_off' => '¿Está seguro que desea deshabilitar el rol seleccionado?',
            'delete' => '¿Está seguro que desea eliminar el rol seleccionado?'
        ],
        'validation' => [
            'name' => 'Nombre del rol usado anteriormente',
            'has_users' => 'El rol esta siendo usado actualmente'
        ],
        'alert' => [
            'delete' => 'Este rol está siendo usada. Imposible eliminar'
        ]
    ]
];
