<?php
// O
return [
    'title' => 'Usuarios',
    'labels' => [
        'create' => 'Crear usuario',
        'new' => 'Nueva usuario',
        'update' => 'Actualizar usuario',
        'edit' => 'Editar usuario',
        'show' => 'Ver usuario',
        'delete' => 'Eliminar usuario',
        'index' => 'Lista de usuarios',
        'username' => 'Nombre de usuario',
        'first_name' => 'Nombres',
        'last_name' => 'Apellidos',
        'document_type' => 'Tipo de identificación',
        'document' => 'Documento',
        'email' => 'Email',
        'photo' => 'Foto',
        'password' => 'Contraseña',
        'repeat_password' => 'Repetir contraseña',
        'status' => 'Estatus',
        'enabled' => 'Activar/Desactivar usuario',
        'roles' => 'Roles',
        'info' => 'Información del usuario',
        'roles_assigned' => 'Roles asignados'
    ],
    'placeholders' => [
        'first_name' => 'Nombres',
        'last_name' => 'Apellidos',
        'document' => 'Documento',
        'email' => 'Email',
        'password' => 'Contraseña',
        'username' => 'Nombre de usuario',
        'repeat_password' => 'Repetir contraseña',
        'document_type' => 'Tipo de documento'
    ],
    'messages' => [
        'success' => [
            'created' => 'Usuario creado exitosamente',
            'updated' => 'Usuario editado exitosamente',
            'deleted' => 'Usuario eliminado exitosamente'
        ],
        'errors' => [
            'create' => 'Ha ocurrido un error al intentar crear el usuario',
            'update' => 'Ha ocurrido un error al intentar editar el usuario'
        ],
        'exceptions' => [
            'not_found' => 'El usuario no existe o no está disponible'
        ],
        'confirm' => [
            'status_on' => '¿Está seguro que desea habilitar el usuario seleccionado?',
            'status_off' => '¿Está seguro que desea deshabilitar el usuario seleccionado?',
            'delete' => '¿Está seguro que desea eliminar el usuario seleccionado?'
        ],
        'validation' => [
            'name' => 'Nombre del usuario usado anteriormente',
            'document_exists' => 'El documento ya existe',
            'username_exists' => 'El nombre de usuario ya existe',
            'email_exists' => 'El correo electrónico ya existe',
            'extension' => 'El archivo debe tener la extensión jpg ó jpeg ó png',
            'password_not_equal' => 'Por favor escriba la misma contraseña',
            'extension_type' => 'El archivo debe tener la extensión jpg ó jpeg ó png'
        ],
        'alert' => [
            'delete' => 'Este usuario está siendo usada. Imposible eliminar'
        ]
    ]
];
