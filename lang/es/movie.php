<?php
// O
return [
    'title' => 'Películas',
    'labels' => [
        'create' => 'Crear película',
        'new' => 'Nueva película',
        'update' => 'Actualizar película',
        'edit' => 'Editar película',
        'show' => 'Ver película',
        'delete' => 'Eliminar película',
        'index' => 'Lista de películas',
        'enabled' => 'Activar/ Desactivar película',
        'description' => 'Descripción',
        'name' => 'Nombre',
        'status' => 'Estado'
    ],
    'placeholders' => [
        'name' => 'Nombre',
        'status' => 'Estado'
    ],
    'messages' => [
        'success' => [
            'created' => 'Película creada exitosamente',
            'updated' => 'Película editada exitosamente',
            'deleted' => 'Película eliminada exitosamente'
        ],
        'errors' => [
            'create' => 'Ha ocurrido un error al intentar crear la película',
            'update' => 'Ha ocurrido un error al intentar editar la película'
        ],
        'exceptions' => [
            'not_found' => 'La película no existe o no está disponible'
        ],
        'confirm' => [
            'status_on' => '¿Está seguro que desea habilitar la película seleccionada?',
            'status_off' => '¿Está seguro que desea deshabilitar la película seleccionada?',
            'delete' => '¿Está seguro que desea eliminar la película seleccionada?'
        ],
        'validation' => [
            'name' => 'Nombre de la película usado anteriormente'
        ],
        'alert' => [
            'delete' => 'Esta película está siendo usada. Imposible eliminar'
        ]
    ]
];
