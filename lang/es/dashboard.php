<?php
// O
return [
    'title' => 'Inicio',
    'labels' => [
        'create' => 'Crear menu',
        'new' => 'Nueva menu',
        'update' => 'Actualizar menu',
        'edit' => 'Editar menu',
        'show' => 'Ver menu',
        'delete' => 'Eliminar menu',
        'index' => 'Lista de menus',
        'name' => 'Nombre',
        'enabled' => 'Activar/ Desactivar',
        'description' => 'Descripción',
        'header' => 'Header',
        'label' => 'Label',
        'slug' => 'Slug',
        'icon' => 'Icon',
        'weight' => 'Weight'
    ],
    'placeholders' => [
        'header' => 'Header',
        'label' => 'Label',
        'slug' => 'Slug',
        'icon' => 'Icon',
        'weight' => 'Weight'
    ],
    'messages' => [
        'success' => [
            'created' => 'Menu creado exitosamente',
            'updated' => 'Menu editado exitosamente',
            'deleted' => 'Menu eliminado exitosamente'
        ],
        'errors' => [
            'create' => 'Ha ocurrido un error al intentar crear el menu',
            'update' => 'Ha ocurrido un error al intentar editar el menu'
        ],
        'exceptions' => [
            'not_found' => 'El menu no existe o no está disponible'
        ],
        'confirm' => [
            'status_on' => '¿Está seguro que desea habilitar el menu seleccionado?',
            'status_off' => '¿Está seguro que desea deshabilitar el menu seleccionado?',
            'delete' => '¿Está seguro que desea eliminar el menu seleccionado?'
        ],
        'validation' => [
            'name' => 'Nombre del menu usado anteriormente'
        ],
        'alert' => [
            'delete' => 'Este menu está siendo usada. Imposible eliminar'
        ]
    ]
];
