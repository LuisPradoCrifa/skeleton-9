/* Vars */
$body = $('body')
$loading = $('#loading-spinner')
$modal = $('#modal-lg')
$modal_sm = $('#modal-sm')
$modal_xl = $('#modal-xl')
$main_content = $('#main_content')
$loadingCount = 0
let $delayToasts = 2500
let $delayLoader = 500
let class_id

const notify = ($title, $class, $body) => {
    $(document).Toasts('create', {
        title: $title,
        class: 'bg-' + $class,
        autohide: true,
        delay: $delayToasts,
        body: $body,
        animationSpeed: 300,
        accordion: true,
        expandSidebar: false
    })
}

// Show loading div pages
const showLoading = () => {
    $('.preloader').css({'height': '100vh'})
    $('.preloader').show()
    $('.animation__shake').show()
}

// Hide loading div pages
const hideLoading = () => {
    setTimeout(function () {
        $('.preloader').css({'height': '0'})
        $('.preloader').hide()
        $('.animation__shake').hide()
    }, $delayLoader)
}

/* Ajaxify */
function pushRequest(url, target, callback, method, data, scrollTop = true, options = null) {
    let config = {
        type: method || 'get',
        data: data || {},
        beforeSend: function () {
            showLoading()
        }
    };
    if (options && options.file === true) {
        config.processData = false
        config.contentType = false
        config.enctype = 'multipart/form-data'
    } else if (options && options.form === true) {
        config.processData = false
        config.contentType = false
    }
    $('#returnBtn').hide()
    $('#returnBtn').removeAttr('attr_url')
    $.ajax(url, config).done(function (response) {
        processResponse(response, target, callback)
        $('body').removeClass('loaded')
    }).fail(function (request, error) {
        hideLoading()
        notify('Error!', 'danger', 'Ha ocurrido un error al intentar realizar la transacci&#243;n')
        console.error(error)
    }).always(function () {
        hideLoading()
        if (scrollTop) {
            $('html, body').animate({scrollTop: 0}, 500)
        }
    })
}

function processResponse(response, target, callback) {
    if (response.no_auth) {
        window.location = '/login'
    }
    if (response.view) {
        target = target || '#main_content'
        let $target = $(target)
        $target.empty().html(response.view)
    } else if (response.modal) {
        let $target_md = $modal.find('.modal-content')
        $target_md.html(response.modal)
        $modal.modal('show')
    } else if (response.modal_sm) {
        let $target_sm = $modal_sm.find('.modal-content')
        $target_sm.html(response.modal_sm)
        $modal_sm.modal('show')
    } else if (response.modal_xl) {
        let $target_xl = $modal_xl.find('.modal-content')
        $target_xl.html(response.modal_xl)
        $modal_xl.modal('show')
    }
    if (response.message) {
        let message = response.message;
        notify(message.title, message.type, message.text)
    }
    if (response.exception) {
        let exception = response.exception;
        notify(message.title, message.type, message.text)
        console.log(exception);
    }
    typeof callback === 'function' && callback(response)
    typeof setContentHeight === 'function' && setContentHeight()
}

// Body ajaxify
$('body').on('click', '.ajaxify', function (e) {
    e.preventDefault()
    let url = $(this).attr('href') || $(this).attr('data-href')
    if (!url)
        return
    let target = $(this).attr('data-ajaxify') || '#main_content'
    pushRequest(url, target)
})

// Redirect history template
const urlHistory = (url) => {
    if (!url)
        return
    pushRequest(url)
}

/* Bootbox Modal */

function confirmModal(message, callback, callback_cancel = null) {
    bootbox.confirm({
        title: '<i class="fa fa-exclamation-circle"></i> Confirmación',
        backdrop: true,
        className: 'bootbox-modal',
        closeButton: false,
        buttons: {
            confirm: {
                label: 'Aceptar',
                className: 'btn-success'
            },
            cancel: {
                label: 'Cancelar',
                className: 'btn-danger'
            },
        },
        message: message,
        callback: function (result) {
            if (result) {
                typeof callback === 'function' && callback()
            } else {
                typeof callback_cancel === 'function' && callback_cancel()
            }
        }
    })
}

function messageModal(message, callback) {
    bootbox.alert({
        title: '<i class="fa fa-exclamation-circle"></i> Mensaje',
        backdrop: true,
        className: 'bootbox-modal',
        closeButton: false,
        buttons: {
            ok: {
                label: 'Aceptar',
                className: 'btn-success'
            }
        },
        message: message,
        callback: function (result) {
            if (result) {
                callback()
            }
        }
    })
}

/* Form validation methods */

let onlyNumbersRegex = new RegExp('^\\d+$')
let alphanumericRegex = new RegExp('^[a-zA-Z0-9]*$')

$.validator.addMethod("onlyIntegers", function (value, element) {
    if (value !== '') {
        return onlyNumbersRegex.test(value)
    } else {
        return true
    }
}, 'Por favor, ingrese solamente números enteros')

$.validator.addMethod('lettersOnly', function (value, element) {
    if (value !== '') {
        return this.optional(element) || /^[a-zA-ZáéíóúüñÁÉÍÓÚÜÑ\s]+$/i.test(value)
    } else {
        return true
    }
}, 'Por favor, ingrese solamente letras')

$.validator.addMethod('emailChecker', function (value) {
    if (value !== '') {
        const email = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/
        return email.test(value)
    } else {
        return true
    }
}, 'Por favor, ingrese una dirección de correo válida')

$.validator.addMethod("passport", function (value) {
    if (value !== '') {
        return alphanumericRegex.test(value)
    } else {
        return true
    }
}, 'Por favor, ingrese un pasaporte válido')

$.validator.addMethod('cedula', function (value) {
    if (value !== '') {
        let provinceCode = value.substring(0, 2);
        if ((provinceCode >= 1 && provinceCode <= 24) || provinceCode === 30) {
            let [sum, mul, index] = [0, 1, value.length];
            while (index--) {
                let num = value[index] * mul;
                sum += num - (num > 9) * 9;
                mul = 1 << index % 2;
            }
            return (sum % 10 === 0) && (sum > 0) && value.length === 10;
        } else {
            return false
        }
    } else {
        return true
    }
}, 'Por favor, ingrese una cédula válida')

$.validator.addMethod("ruc", function (value) {
    if (value !== '') {
        return onlyNumbersRegex.test(value) && value.length === 13
    } else {
        return true;
    }
}, 'Por favor, ingrese un RUC válido')

// Regular expresion: Minimum eight characters, at least one uppercase letter, one lowercase letter and one number:
$.validator.addMethod('passwordCode', function (value) {
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d\w\W]{8,}$/.test(value)
}, 'La contraseña debe contener al menos una mayúscula, una minúscula, un número y mínimo ocho caracteres')

const append_options_in_select_from_json_data = (
    selectTarget,
    jsonData,
    defaultSelectValue = null) => {
    let $selectTarget = $('#' + selectTarget)
    $('#' + selectTarget + ' option').remove()
    $selectTarget.append($("<option></option>"))

    // Document ready (This is for templates blade for update)
    if (defaultSelectValue != null) {
        $selectTarget.append($("<option></option>")
            .attr("value", defaultSelectValue.id)
            .text(defaultSelectValue.name))
            .attr('selected')
    }
    $.each(jsonData, function (index, option) {
        if (defaultSelectValue && option.id != defaultSelectValue.id) {
            $selectTarget.append($("<option></option>")
                .attr("value", option.id)
                .text(option.name))
        } else {
            $selectTarget.append($("<option></option>")
                .attr("value", option.id)
                .text(option.name))
        }
    });
}
/* Form */
$validateDefaults = {
    ignore: [],
    rules: {},
    messages: {},
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback')
        element.closest('.form-group').append(error)
    },
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid')
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid')
    }
}

$formAjaxDefaults = {
    dataType: 'json',
    beforeSubmit: function (formData, jqForm) {
        if (jqForm.valid()) {
            showLoading()
            return true
        }
        return false
    },
    success: function (response) {
        processResponse(response, '#main_content', function () {
            $validateDefaults.rules = {}
            $validateDefaults.messages = {}
        }, true)
    },
    error: function (param1, param2, param3) {
        notify('Error!', 'danger', 'Ha ocurrido un error al intentar realizar la transacci&#243;n')
        $validateDefaults.rules = {}
        $validateDefaults.messages = {}
        console.log(param3)
    },
    complete: function () {
        hideLoading()
    }
}

// Clean form validations
function cleanFormValidations(formId) {
    $('.has-error', $('#' + formId)).removeClass('has-error')
    $('.has-success', $('#' + formId)).removeClass('has-success')
    $('.help-block', $('#' + formId)).remove()
}

$.extend($.validator.messages, {
    required: "Este campo es requerido.",
    remote: "Por favor, llene este campo.",
    email: "Por favor, escriba una dirección de correo válida.",
    url: "Por favor, escriba una URL válida.",
    date: "Por favor, escriba una fecha válida.",
    dateISO: "Por favor, escriba una fecha (ISO) válida.",
    number: "Por favor, escriba un número válido.",
    digits: "Por favor, escriba sólo dígitos.",
    creditcard: "Por favor, escriba un número de tarjeta válido.",
    equalTo: "Por favor, escriba el mismo valor de nuevo.",
    extension: "Por favor, escriba un valor con una extensión aceptada.",
    maxlength: $.validator.format("Por favor, no escriba más de {0} caracteres."),
    minlength: $.validator.format("Por favor, no escriba menos de {0} caracteres."),
    rangelength: $.validator.format("Por favor, escriba un valor entre {0} y {1} caracteres."),
    range: $.validator.format("Por favor, escriba un valor entre {0} y {1}."),
    max: $.validator.format("Por favor, escriba un valor menor o igual a {0}."),
    min: $.validator.format("Por favor, escriba un valor mayor o igual a {0}."),
    cedCR: "Por favor, escriba el número de cédula válido."
})

/* Logout */
$body.on('click', '.logout', function (e) {
    e.preventDefault()
    sessionStorage.clear()
    $('#logout-form').submit()
});

/**
 * Datatables
 **/
(function ($, DataTable) {
    $.extend(true, DataTable.defaults, {
        dom: '<lf<t>ipr>',
        bProcessing: true,
        bServerSide: true,
        bAutoWidth: false,
        responsive: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        oLanguage: {
            "sProcessing": "<i class='fa fa-spinner fa-spin'></i>",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ning&#250;n dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "<i class='fa fa-spinner fa-spin'></i>",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "&#218;ltimo",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
})(jQuery, jQuery.fn.DataTable)
