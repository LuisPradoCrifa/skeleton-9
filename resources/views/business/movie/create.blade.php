@can('movie-create')
<div class="modal-header">
    <h4 class="modal-title">{{ __('movie.labels.create') }}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form id="createMenu" role="form" action="{{ route('store.movie') }}" method="post" class="form-horizontal form-label-left">
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <label for="header" class="col-sm-2 col-form-label">{{ __('movie.labels.name') }}</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="name" placeholder="{{ __('movie.placeholders.name') }}">
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <div class="row">
                <div class="col text-center">
                    <button type="button" class="btn btn-default btn-sm"
                            data-dismiss="modal">{{ __('app.labels.close') }}</button>
                    <button type="submit"
                            class="btn btn-primary btn-sm">{{ __('app.labels.save') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(function () {
        const $form = $('#createMenu')

        $validateDefaults.rules = {
            name: {
                required: true,
                remote: {
                    url: "{!! route('checkuniquefield') !!}",
                    data: {
                        fieldName: 'name',
                        fieldValue: () => {
                            return $('#name', $form).val()
                        },
                        model: 'App\\Models\\Business\\Movie'
                    }
                }
            }
        }

        $validateDefaults.messages = {
            name: {
                remote: '{!! trans('movie.messages.validation.name') !!}'
            }
        }

        // J Validator
        $form.validate($validateDefaults);
        $form.ajaxForm($.extend(false, $formAjaxDefaults, {
            success: (response) => {
                processResponse(response, null, () => {
                    $modal.modal('hide')
                })
            }
        }))
    })
</script>
@else
    @include('errors.403_modal')
@endcan
