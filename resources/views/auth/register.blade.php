<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('_APP_NAME') }}</title>
    <link rel="icon" type="image/x-icon" href="{{ env('ICON') }}">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('backend/dist/css/adminlte.min.css') }}">
</head>
<body class="hold-transition register-page">
@inject('User', '\App\Models\User')
<div class="register-box">
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a href="{{ asset('images/logo_sloncorp_login.png') }}" class="h1"><b>{{ env('_APP_NAME') }}</b></a>
        </div>
        <div class="card-body">
            <p class="login-box-msg">{{ __('app.labels.new_user') }}</p>
            <form id="registerUser" role="form" action="{{ route('register') }}" method="post" class="form-horizontal form-label-left">
                @csrf
                <div class="form-group input-group mb-3">
                    <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus placeholder="{{ __('user.placeholders.first_name') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                    @error('first_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group input-group mb-3">
                    <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus placeholder="{{ __('user.placeholders.last_name') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                    @error('last_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group input-group mb-3">
                    <select id="document_type" class="form-control @error('document_type') is-invalid @enderror" name="document_type" required>
                        <option value="">{{ __('user.placeholders.document_type') }}</option>
                        @foreach($User::DOCUMENT_TYPE as $value)
                            <option value="{{ $value['id'] }}">
                                {{ $value['description'] }}
                            </option>
                        @endforeach
                    </select>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-bars"></span>
                        </div>
                    </div>
                    @error('document_type')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group input-group mb-3">
                    <input id="document" type="text" class="form-control @error('document') is-invalid @enderror" name="document" value="{{ old('document') }}" required autocomplete="document" autofocus placeholder="{{ __('user.placeholders.document') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-id-card"></span>
                        </div>
                    </div>
                    @error('document')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group input-group mb-3">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('user.placeholders.email') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group input-group mb-3">
                    <input id="username" type="email" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" placeholder="{{ __('user.placeholders.username') }}" readonly>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-id-card-alt"></span>
                        </div>
                    </div>
                    @error('username')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group input-group mb-3">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="{{ __('user.placeholders.password') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group input-group mb-3">
                    <input id="repeat_password" type="password" class="form-control" name="repeat_password" required autocomplete="repeat_password" placeholder="{{ __('user.placeholders.repeat_password') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-7">
                        <div class="icheck-primary">
                            <input type="checkbox" id="agreeTerms" name="terms" value="agree" required>
                            <label for="agreeTerms">
                                {{ __('app.labels.accept_terms') }} <a href="#">{{ __('app.labels.terms') }}</a>
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-5">
                        <button type="submit" class="btn btn-primary btn-block">{{ __('app.labels.register') }}</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">{{ __('app.labels.i_already_have_a_membership') }}</a>
        </div>
        <!-- /.form-box -->
    </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="{{ asset('backend/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('backend/dist/js/adminlte.min.js') }}"></script>
<!-- jquery-validation -->
<script src="{{ asset('js/jquery-form/dist/jquery.form.min.js') }}"></script>
<script src="{{ asset('js/jquery-form/dist/jquery.form.min.js.map') }}" type="application/json"></script>
<script src="{{ asset('backend/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-validation/localization/messages_es.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-validation/additional-methods.min.js') }}"></script>
<!-- DataTables  & Plugins -->
<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('backend/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('backend/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- Custom JS -->
<script src="{{ asset('backend/js/main.js') }}"></script>
<script>
    $(function () {

        const $form = $('#registerUser')
        const documentType = $('#document_type')
        const document = $('#document')
        const email = $('#email')
        const userName = $('#username')

        email.on('change', function () {
            userName.val(email.val())
        })

        documentType.on('change', function () {
            switch (this.value) {
                case 'CED' :
                    document.rules("remove", "passport ruc")
                    document.rules("add", {
                        cedula: true
                    })
                    break;
                case 'PAS' :
                    document.rules("remove", "cedula ruc")
                    document.rules("add", {
                        passport: true
                    })
                    break;
                case 'RUC' :
                    document.rules("remove", "cedula passport")
                    document.rules("add", {
                        ruc: true
                    })
            }
            document.valid()
        })

        $validateDefaults.rules = {
            first_name: {
                required: true,
                lettersOnly: true
            },
            last_name: {
                required: true,
                lettersOnly: true
            },
            document_type: {
                required: true
            },
            document: {
                required: true
            },
            email: {
                required: true,
                emailChecker: true
            },
            username: {
                required: true
            },
            password: {
                required: true,
                minlength: 6
            },
            repeat_password: {
                required: true,
                equalTo: '#password'
            },
            agreeTerms: {
                required: true
            }
        }

        $validateDefaults.messages = {
            email: {
                remote: '{!! trans('user.messages.validation.email_exists') !!}'
            },
            username: {
                remote: '{!! trans('user.messages.validation.name') !!}'
            },
            document: {
                remote: '{!! trans('user.messages.validation.document_exists') !!}'
            }
        }

        // J Validator
        $form.validate($validateDefaults);

        @if(isset($response1) && $response1)
            notify('Alerta!', 'success', '{{ $response1 }}')
        @endif
    })
</script>
</body>
</html>
