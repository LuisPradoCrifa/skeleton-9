<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('_APP_NAME') }}</title>
    <link rel="icon" type="image/x-icon" href="{{ env('ICON') }}">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('backend/dist/css/adminlte.min.css') }}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a href="#" class="h1"><b>{{ env('_APP_NAME') }}</b></a>
        </div>
        <div class="card-body">
            <p class="login-box-msg">{{ __('auth.update_password') }}</p>
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form action="{{ route('reset-password') }}" method="post" autocomplete="off">
                @csrf
                @method('PUT')

                <input type="hidden" name="email" value="{{ $email }} "/>

                <div class="input-group mb-3">
                    <input type="password" name="password"
                           class="form-control {{$errors->first('password') ? 'is-invalid' : ''}}"
                           value="{{ old('password') }}" placeholder="{{ __('user.placeholders.password') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-key"></span>
                        </div>
                    </div>
                    {!! $errors->first('password', '<div class="invalid-feedback">:message</div>') !!}
                </div>
                <div class="input-group mb-3">
                    <input type="password" name="confirm_password"
                           class="form-control {{$errors->first('confirm_password') ? 'is-invalid' : ''}}"
                           value="{{ old('confirm_password') }}" placeholder="{{ __('user.placeholders.repeat_password') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-key"></span>
                        </div>
                    </div>
                    {!! $errors->first('confirm_password', '<div class="invalid-feedback">:message</div>') !!}
                </div>
                <div class="row mb-0">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block"> {{ __('app.labels.change_password') }}</button>
                    </div>
                </div>
            </form>
            <p class="mt-3 mb-1">
                <a href="{{ route('login') }}">{{ __('app.labels.login_page') }}</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('backend/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('backend/dist/js/adminlte.min.js') }}"></script>
<!-- DataTables  & Plugins -->
<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('backend/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('backend/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('backend/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- jquery-validation -->
<script src="{{ asset('js/jquery-form/dist/jquery.form.min.js') }}"></script>
<script src="{{ asset('js/jquery-form/dist/jquery.form.min.js.map') }}" type="application/json"></script>
<script src="{{ asset('backend/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-validation/localization/messages_es.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-validation/additional-methods.min.js') }}"></script>
<script src="{{ asset('js/bootbox/bootbox.js') }}"></script>
<!-- bs-custom-file-input -->
<script src="{{ asset('backend/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<script>
    @if(isset($message) && $message)
    $(document).Toasts('create', {
        title: '{!! $message['title'] !!}!',
        class: 'bg-{!! $message['type'] !!}',
        autohide: true,
        delay: 5000,
        body: '{!! $message['message'] !!}',
        animationSpeed: 100,
        accordion: true,
        expandSidebar: false
    })
    @endif
</script>

</body>
</html>
