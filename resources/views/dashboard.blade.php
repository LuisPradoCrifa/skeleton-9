<!-- Navbar content -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('dashboard.title') }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">{{ __('dashboard.title') }}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- COLOR PALETTE -->
        <div class="card card-default color-palette-box">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="fas fa-home"></i>
                    {{ __('dashboard.title') }}
                </h3>
            </div>
            <!-- /.card-header -->
            <!-- .card-body -->
            <div class="card-body">

            </div>
            <!-- /.card-body -->
            <div class="card-footer">&nbsp&nbsp</div>
        </div>
    </div>
</section>
