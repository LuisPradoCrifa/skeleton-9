<!-- Left navbar links -->
<ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
</ul>

<!-- Right navbar links -->
<ul class="navbar-nav ml-auto">
    <!-- Navbar Search -->
    <li class="nav-item">
{{--        TODO: Search bar--}}
{{--        <a class="nav-link" data-widget="navbar-search" href="#" role="button">--}}
{{--            <i class="fas fa-search"></i>--}}
{{--        </a>--}}
        <div class="navbar-search-block">
            <form class="form-inline">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                        <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </li>

    <!-- User Dropdown Menu -->
    <li class="nav-item dropdown user-menu" id="profileUser">
        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
            <img @if(currentUser()->photo) src="{{ asset('images/images/'. currentUser()->id .'/'. currentUser()->photo) }}" @else src="{{ asset('images/user.png') }}" @endif class="user-image img-circle elevation-2" alt="User Image">
            <span class="d-none d-md-inline">{{ currentUser()->fullName() }}</span>
        </a>
        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <!-- User image -->
            <li class="user-header bg-primary">
                <img @if(currentUser()->photo) src="{{ asset('images/images/'. currentUser()->id .'/'. currentUser()->photo) }}" @else src="{{ asset('images/user.png') }}" @endif class="img-circle elevation-2" alt="User Image">
                <p>
                    {{ currentUser()->fullName() }}
                    <small>Miebro desde: {{ currentUser()->created_at }}</small>
                </p>
            </li>
            <!-- Menu Body -->
            @can('profile-change_password')
                <li class="user-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            <a href="{{ route('change_password.profile') }}" class="ajaxify">
                                <i class="fas fa-key mr-2"></i> {{ __('app.labels.change_password') }}
                            </a>
                        </div>
                    </div>
                    <!-- /.row -->
                </li>
            @endcan
            <!-- Menu Footer-->
            <li class="user-footer">
                @can('profile-list')
                    <a href="{{ route('index.profile') }}" class="btn btn-default btn-fla ajaxify">{{ __('app.labels.profile') }}</a>
                @endcan
                <a href="#" class="btn btn-default btn-flat float-right logout">{{ __('app.labels.exit') }}</a>
            </li>
        </ul>
    </li>

    <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
            <i class="fas fa-th-large"></i>
        </a>
    </li>
</ul>
