@if(is_array($menu))
    <li class="nav-item">
        <a href="{{ (array_key_exists('slug', $menu)) ? route($menu['slug']) : '#' }}" class="nav-link {{ (array_key_exists('slug', $menu)) ? 'ajaxify' : '' }}" @if(isset($menu['slug']) && $menu['slug']) attr_route="{{ $menu['slug'] }}" @else attr_route="null" @endif>
            @if (array_key_exists('icon', $menu))
                <i class="fa fa-{{$menu['icon']}}"></i>
            @endif
            <p>
                {{ $menu['label'] }}
                @if(array_key_exists('children', $menu))
                    <i class="right fas fa-angle-left"></i>
                @endif
            </p>
        </a>
        @if(array_key_exists('children', $menu))
        <ul class="nav nav-treeview">
            @each('template.menu', $menu['children'], 'menu')
        </ul>
        @endif
    </li>
@endif
