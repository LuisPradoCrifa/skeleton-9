@can('user-show')
@inject('User', '\App\Models\User')
<div class="modal-header">
    <h4 class="modal-title">{{ __('user.labels.show') }}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form id="createMenu" role="form" method="post" class="form-horizontal form-label-left">
        <div class="card-body">
            <div class="form-group row">
                <label for="header" class="col-sm-3 col-form-label">{{ __('user.labels.first_name') }}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" placeholder="{{ __('user.placeholders.first_name') }}" value="{{ $entity->first_name }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="label" class="col-sm-3 col-form-label">{{ __('user.labels.last_name') }}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" placeholder="{{ __('user.placeholders.last_name') }}" value="{{ $entity->last_name }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="document_type" class="col-sm-3 col-form-label">{{ __('user.labels.document_type') }}</label>
                <div class="col-sm-9">
                    <select class="form-control select2" disabled>
                        <option></option>
                        @foreach($User::DOCUMENT_TYPE as $value)
                            <option value="{{ $value['id'] }}" @if($value['id'] === $entity->document_type) selected @endif>
                                {{ $value['description'] }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="document" class="col-sm-3 col-form-label">{{ __('user.labels.document') }}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" placeholder="{{ __('user.placeholders.document') }}" value="{{ $entity->document }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="label" class="col-sm-3 col-form-label">{{ __('user.labels.email') }}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" placeholder="{{ __('user.placeholders.email') }}" value="{{ $entity->email }}" disabled>
                </div>
            </div>
            @if(count($entity->roles))
                <div class="form-group text-center row">
                    <div class="col-md-8 offset-2">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">{{ __('user.labels.roles') }}</h3>
                            </div>
                            <div class="card-body p-0">
                                <table class="table table-sm">
                                    <thead>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>{{ __('role.labels.name') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($entity->roles as $role)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $role->name }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <div class="row">
                <div class="col text-center">
                    <button type="button" class="btn btn-default btn-sm"
                            data-dismiss="modal">{{ __('app.labels.close') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $(function () {
        $('.select2').select2({
            placeholder: "{{ trans('app.labels.select') }}",
            width: '100%',
            theme: "classic",
            allowClear: true
        }).on("select2:close", function (e) {
            $(this).valid()
        })
    })
</script>
@else
    @include('errors.403_modal')
@endcan
