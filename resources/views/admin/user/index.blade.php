@can('user-list')
    <!-- Navbar content -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('user.title') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('app.labels.administration') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('user.title') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- COLOR PALETTE -->
            <div class="card card-default color-palette-box">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-users"></i>
                        {{ __('user.title') }}
                    </h3>
                    @can('user-create')
                        <a href="{{ route('users.create') }}"
                           class="btn btn-primary bg-gradient-primary btn-sm float-right ajaxify">
                            {{ __('user.labels.create') }}
                        </a>
                    @endcan
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="user_tb" class="table table-striped table-bordered" width="100%">
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{ __('user.labels.username') }}</th>
                            <th>{{ __('user.labels.first_name') }}</th>
                            <th>{{ __('user.labels.document') }}</th>
                            <th>{{ __('user.labels.email') }}</th>
                            <th>{{ __('user.labels.status') }}</th>
                            <th>{{ __('app.labels.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">&nbsp&nbsp</div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

    <script>
        $(function () {
            let datatable = $('#user_tb').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('users.data') }}",
                columns: [
                    {data: 'id', name: 'id', visible: false, sortable: false, searchable: false, width: '0'},
                    {data: 'username', name: 'username', width: '20%', sortable: true, searchable: true},
                    {data: 'full_name', name: 'full_name', width: '20%', sortable: true, searchable: true},
                    {data: 'document', name: 'document', width: '10%', sortable: true, searchable: true},
                    {data: 'email', name: 'email', width: '25%', sortable: true, searchable: true},
                    {data: 'enabled', width: '10%', searchable: false, sortable: false},
                    {data: 'actions', width: '15%', sortable: false, searchable: false, class: 'text-center'}
                ]
            })

            $('#user_tb').on('click', 'input[type="checkbox"]', function (e) {
                e.preventDefault()
                let confirmMessage = $(this).is(':checked') ? '{{ __('user.messages.confirm.status_on') }}' : '{{ __('user.messages.confirm.status_off') }}'
                let element = $(this)
                confirmModal(confirmMessage, function () {
                    let id = element.closest('tr').attr('id')
                    let url = "{!! route('users.enable_disable', ['id' => '__ID__']) !!}"
                    url = url.replace('__ID__', id)
                    pushRequest(url, null, function () {
                        datatable.draw()
                    }, 'get', null)
                })
            })
        })
    </script>

@else
    @include('errors.403')
@endcan
