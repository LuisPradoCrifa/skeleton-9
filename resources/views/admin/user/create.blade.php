@can('user-create')
@inject('User', '\App\Models\User')
<div class="modal-header">
    <h4 class="modal-title">{{ __('user.labels.create') }}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form id="createMenu" role="form" action="{{ route('store.users') }}" method="post" class="form-horizontal form-label-left">
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <label for="first_name" class="col-sm-3 col-form-label">
                    {{ __('user.labels.first_name') }} <span class="text-danger">*</span>
                </label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="first_name" id="first_name" maxlength="50" placeholder="{{ __('user.placeholders.first_name') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="last_name" class="col-sm-3 col-form-label">
                    {{ __('user.labels.last_name') }} <span class="text-danger">*</span>
                </label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="last_name" id="last_name" maxlength="50" placeholder="{{ __('user.placeholders.last_name') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="document_type" class="col-sm-3 col-form-label">
                    {{ __('user.labels.document_type') }} <span class="text-danger">*</span>
                </label>
                <div class="col-sm-9">
                    <select id="document_type" name="document_type" class="form-control select2" required>
                        <option></option>
                        @foreach($User::DOCUMENT_TYPE as $value)
                            <option value="{{ $value['id'] }}">
                                {{ $value['description'] }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="document" class="col-sm-3 col-form-label">
                    {{ __('user.labels.document') }} <span class="text-danger">*</span>
                </label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="document" id="document" maxlength="50" placeholder="{{ __('user.placeholders.document') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-sm-3 col-form-label">
                    {{ __('user.labels.email') }}  <span class="text-danger">*</span>
                </label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="email" id="email" placeholder="{{ __('user.placeholders.email') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="username" class="col-sm-3 col-form-label">
                    {{ __('user.labels.username') }} <span class="text-danger">*</span>
                </label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="username" id="username" placeholder="{{ __('user.placeholders.username') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-sm-3 col-form-label">
                    {{ __('user.labels.password') }} <span class="text-danger">*</span>
                </label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="password" id="password" placeholder="{{ __('user.placeholders.password') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="repeat_password" class="col-sm-3 col-form-label">
                    {{ __('user.labels.repeat_password') }} <span class="text-danger">*</span>
                </label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="repeat_password" id="repeat_password" placeholder="{{ __('user.placeholders.repeat_password') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="repeat_password" class="col-sm-3 col-form-label">
                    {{ __('user.labels.roles') }} <span class="text-danger">*</span>
                </label>
                <div class="col-sm-9">
                    <select id="roles" name="roles[]" class="form-control select2" multiple required>
                        <option></option>
                        @foreach($roles as $value)
                            <option value="{{ $value->id }}">
                                {{ $value->description }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <div class="row">
                <div class="col text-center">
                    <button type="button" class="btn btn-default btn-sm"
                            data-dismiss="modal">{{ __('app.labels.close') }}</button>
                    <button type="submit"
                            class="btn btn-primary btn-sm">{{ __('app.labels.save') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(function () {
        const $form = $('#createMenu')
        const documentType = $('#document_type')
        const document = $('#document')
        const email = $('#email')
        const userName = $('#username')

        $('.select2', $form).select2({
            placeholder: "{{ trans('app.labels.select') }}",
            width: '100%',
            theme: "classic",
            allowClear: true
        }).on("select2:close", function (e) {
            $(this).valid()
        })

        email.on('change', function () {
            userName.val(email.val())
        })

        documentType.on('change', function () {
            switch (this.value) {
                case 'CED' :
                    document.rules("remove", "passport ruc")
                    document.rules("add", {
                        cedula: true
                    })
                    break;
                case 'PAS' :
                    document.rules("remove", "cedula ruc")
                    document.rules("add", {
                        passport: true
                    })
                    break;
                case 'RUC' :
                    document.rules("remove", "cedula passport")
                    document.rules("add", {
                        ruc: true
                    })
            }
            document.valid()
        })

        $validateDefaults.rules = {
            first_name: {
                required: true,
                lettersOnly: true
            },
            last_name: {
                required: true,
                lettersOnly: true
            },
            document_type: {
                required: true
            },
            document: {
                required: true,
                remote: {
                    url: "{!! route('checkuniquefield') !!}",
                    data: {
                        fieldName: 'document',
                        fieldValue: () => {
                            return $('#document', $form).val()
                        },
                        model: 'App\\Models\\User'
                    }
                }
            },
            email: {
                required: true,
                emailChecker: true,
                remote: {
                    url: "{!! route('check_email.users') !!}",
                    data: {
                        email: function () {
                            return email.val()
                        }
                    }
                }
            },
            username: {
                required: true,
                remote: {
                    url: "{!! route('checkuniquefield') !!}",
                    data: {
                        fieldName: 'username',
                        fieldValue: () => {
                            return $('#username', $form).val()
                        },
                        model: 'App\\Models\\User'
                    }
                }
            },
            password: {
                required: true,
                minlength: 6
            },
            repeat_password: {
                required: true,
                equalTo: '#password'
            },
            roles: {
                required: true
            }
        }

        $validateDefaults.messages = {
            email: {
                remote: '{!! trans('user.messages.validation.email_exists') !!}'
            },
            username: {
                remote: '{!! trans('user.messages.validation.name') !!}'
            },
            document: {
                remote: '{!! trans('user.messages.validation.document_exists') !!}'
            }
        }

        // J Validator
        $form.validate($validateDefaults);
        $form.ajaxForm($.extend(false, $formAjaxDefaults, {
            success: (response) => {
                processResponse(response, null, () => {
                    $modal.modal('hide')
                })
            }
        }))
    })
</script>

@else
    @include('errors.403_modal')
@endcan
