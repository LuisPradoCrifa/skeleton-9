@can('menu-create')
<div class="modal-header">
    <h4 class="modal-title">{{ __('menu.labels.create') }}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form id="createMenu" role="form" action="{{ route('store.menus') }}" method="post" class="form-horizontal form-label-left">
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <label for="header" class="col-sm-2 col-form-label">
                    {{ __('menu.labels.header') }}
                </label>
                <div class="col-sm-10">
                    <input type="text" name="header" class="form-control" id="header" placeholder="{{ __('menu.placeholders.header') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="label" class="col-sm-2 col-form-label">
                    {{ __('menu.labels.label') }} <span class="text-danger">*</span>
                </label>
                <div class="col-sm-10">
                    <input type="text" name="label" class="form-control" id="label" placeholder="{{ __('menu.placeholders.label') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="slug" class="col-sm-2 col-form-label">
                    {{ __('menu.labels.slug') }}
                </label>
                <div class="col-sm-10">
                    <input type="text" name="slug" class="form-control" id="slug" placeholder="{{ __('menu.placeholders.slug') }}">
                </div>
            </div>
            <div class="form-group input-group-sm row">
                <label for="icon" class="col-sm-2 col-form-label">
                    {{ __('menu.labels.icon') }}
                </label>
                <div class="col-sm-10">
                    <input type="text" name="icon" class="form-control" id="icon" placeholder="{{ __('menu.placeholders.icon') }}">
                </div>
            </div>
            <div class="form-group input-group-sm row">
                <label for="weight" class="col-sm-2 col-form-label">
                    {{ __('menu.labels.weight') }} <span class="text-danger">*</span>
                </label>
                <div class="col-sm-10">
                    <input type="text" name="weight" class="form-control" id="weight" placeholder="{{ __('menu.placeholders.weight') }}">
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <div class="row">
                <div class="col text-center">
                    <button type="button" class="btn btn-default btn-sm"
                            data-dismiss="modal">{{ __('app.labels.close') }}</button>
                    <button type="submit"
                            class="btn btn-primary btn-sm">{{ __('app.labels.save') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(function () {
        const $form = $('#createMenu')

        $validateDefaults.rules = {
            label: {
                required: true,
                remote: {
                    url: "{!! route('checkuniquefield') !!}",
                    data: {
                        fieldName: 'label',
                        fieldValue: () => {
                            return $('#label', $form).val()
                        },
                        model: 'App\\Models\\admin\\Menu'
                    }
                }
            },
            weight: {
                required: true
            }
        }

        $validateDefaults.messages = {
            label: {
                remote: '{!! trans('menu.messages.validation.name') !!}'
            }
        }

        // J Validator
        $form.validate($validateDefaults);
        $form.ajaxForm($.extend(false, $formAjaxDefaults, {
            success: (response) => {
                processResponse(response, null, () => {
                    $modal.modal('hide')
                })
            }
        }))
    })
</script>
@else
    @include('errors.403_modal')
@endcan
