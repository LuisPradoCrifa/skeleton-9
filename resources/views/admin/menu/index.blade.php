@can('menu-list')
    <!-- Navbar content -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('menu.title') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('app.labels.administration') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('menu.title') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- COLOR PALETTE -->
            <div class="card card-default color-palette-box">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-list"></i>
                        {{ __('menu.title') }}
                    </h3>
                    @can('menu-create')
                        <a href="{{ route('menus.create') }}"
                           class="btn btn-primary bg-gradient-primary btn-sm float-right ajaxify">
                            {{ __('menu.labels.create') }}
                        </a>
                    @endcan
                </div>
                <!-- /.card-header -->
                <!-- .card-body -->
                <div class="card-body">
                    <table id="menu_tb" class="table table-striped table-bordered" width="100%">
                        <thead>
                        <tr>
                            <th></th>
                            <th>label</th>
                            <th>header</th>
                            <th>slug</th>
                            <th>icon</th>
                            <th>weight</th>
                            <th>Enable</th>
                            <th>{{ __('app.labels.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">&nbsp&nbsp</div>
            </div>
        </div>
    </section>
    <script>
        $(function () {
            let datatable = $('#menu_tb').DataTable({
                responsive: true,
                ajax: "{{ route('menus.data') }}",
                columns: [
                    {data: 'id', name: 'id', visible: false, sortable: false, searchable: false, width: '0'},
                    {data: 'label', name: 'label', width: '20%', sortable: true, searchable: true},
                    {data: 'header', name: 'header', width: '20%', sortable: true, searchable: true},
                    {data: 'slug', name: 'slug', width: '15%', sortable: true, searchable: true},
                    {data: 'icon', name: 'icon', width: '10%', sortable: true, searchable: true},
                    {data: 'weight', name: 'weight', width: '10%', sortable: true, searchable: true},
                    {data: 'enabled', width: '10%', searchable: false, sortable: false},
                    {data: 'actions', width: '15%', sortable: false, searchable: false, class: 'text-center'}
                ]
            })

            $('#menu_tb').on('click', 'input[type="checkbox"]', function (e) {
                e.preventDefault()
                let confirmMessage = $(this).is(':checked') ? '{{ __('menu.messages.confirm.status_on') }}' : '{{ __('menu.messages.confirm.status_off') }}'
                let element = $(this)
                confirmModal(confirmMessage, function () {
                    let id = element.closest('tr').attr('id')
                    let url = "{!! route('menus.enable_disable', ['id' => '__ID__']) !!}"
                    url = url.replace('__ID__', id)
                    pushRequest(url, null, function () {
                        datatable.draw()
                    }, 'get', null)
                })
            })

            @if(isset($reload) && $reload)
                confirmModal('{{ __('menu.messages.confirm.reload') }}', () => {
                    location.reload()
                })
            @endif
        })
    </script>
@else
    @include('errors.403')
@endcan
