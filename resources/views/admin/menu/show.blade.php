@can('menu-show')
<div class="modal-header">
    <h4 class="modal-title">{{ __('menu.labels.show') }}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form id="createMenu" role="form" method="post" class="form-horizontal form-label-left">
        <div class="card-body">
            <div class="form-group row">
                <label for="header" class="col-sm-2 col-form-label">{{ __('menu.labels.header') }}</label>
                <div class="col-sm-10">
                    <input type="text" name="header" class="form-control" id="header" placeholder="{{ __('menu.placeholders.header') }}" value="{{ $entity->header }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="label" class="col-sm-2 col-form-label">{{ __('menu.labels.label') }}</label>
                <div class="col-sm-10">
                    <input type="text" name="label" class="form-control" id="label" placeholder="{{ __('menu.placeholders.label') }}" value="{{ $entity->label }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="slug" class="col-sm-2 col-form-label">{{ __('menu.labels.slug') }}</label>
                <div class="col-sm-10">
                    <input type="text" name="slug" class="form-control" id="slug" placeholder="{{ __('menu.placeholders.slug') }}" value="{{ $entity->slug }}" disabled>
                </div>
            </div>
            <div class="form-group input-group-sm row">
                <label for="icon" class="col-sm-2 col-form-label">{{ __('menu.labels.icon') }}</label>
                <div class="col-sm-10">
                    <i class='fa fa-{{ $entity->icon }}' aria-hidden='true'></i>
                </div>
            </div>
            <div class="form-group input-group-sm row">
                <label for="weight" class="col-sm-2 col-form-label">{{ __('menu.labels.weight') }}</label>
                <div class="col-sm-10">
                    <input type="text" name="weight" class="form-control" id="weight" placeholder="{{ __('menu.placeholders.weight') }}" value="{{ $entity->weight }}" disabled>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <div class="row">
                <div class="col text-center">
                    <button type="button" class="btn btn-default btn-sm"
                            data-dismiss="modal">{{ __('app.labels.close') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>
@else
    @include('errors.403_modal')
@endcan
