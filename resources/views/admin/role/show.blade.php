@can('role-show')
<div class="modal-header">
    <h4 class="modal-title">{{ __('role.labels.show') }}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form id="createMenu" role="form" method="post" class="form-horizontal form-label-left">
        <div class="card-body">
            <div class="form-group row">
                <label for="header" class="col-sm-2 col-form-label">{{ __('role.labels.name') }}</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="name" placeholder="{{ __('role.placeholders.name') }}" value="{{ $entity->name }}" disabled>
                </div>
            </div>
            <div class="form-group row">
                <label for="header" class="col-sm-2 col-form-label">{{ __('role.labels.description') }}</label>
                <div class="col-sm-10">
                    <input type="text" name="description" class="form-control" id="description" placeholder="{{ __('role.placeholders.description') }}" value="{{ $entity->description }}" disabled>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <div class="row">
                <div class="col text-center">
                    <button type="button" class="btn btn-default btn-sm"
                            data-dismiss="modal">{{ __('app.labels.close') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>
@else
    @include('errors.403_modal')
@endcan
