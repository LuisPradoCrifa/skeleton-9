@can('role-permissions')
<div class="modal-header">
    <h4 class="modal-title">{{ __('role.labels.permissions') }}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form id="updateMenu" role="form" action="{{ route('update.roles', ['id' => $entity->id]) }}" method="post" class="form-horizontal form-label-left">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group row">
                <label for="header" class="col-sm-2 col-form-label">{{ __('role.labels.name') }}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control"placeholder="{{ __('role.placeholders.name') }}" value="{{ $entity->description }}" disabled>
                </div>
            </div>
            <div class="row h-100 justify-content-center align-items-center">
                <h3 class="card-title text-center">
                    <strong>{{ __('role.labels.permissions') }}</strong>
                </h3>
            </div>
            <div class="form-group row">
                <div class="form-group">
                    @foreach($permission as $value)
                            @if(!$value->parent_id)
                                <div class="custom-control custom-switch custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input select-all" attr_id="{{ $value->id }}" name="permission[]" id="permission-{{ $value->id }}" value="{{ $value->id }}" @if(in_array($value->id, $rolePermissions)) checked @endif>
                                    <label class="custom-control-label" for="permission-{{ $value->id }}">{{ $value->description }}</label>
                                </div>
                            @else
                                <div class="custom-control custom-switch">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" class="custom-control-input select-individual attr_parent_id_{{ $value->parent_id }}" attr_parent_id="{{ $value->parent_id }}" name="permission[]" id="permission-{{ $value->id }}" value="{{ $value->id }}" @if(in_array($value->id, $rolePermissions)) checked @endif>
                                    <label class="custom-control-label" for="permission-{{ $value->id }}">{{ $value->description }}</label>
                                </div>
                            @endif
                    @endforeach
                </div>
            </div>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <div class="row">
                <div class="col text-center">
                    <button type="button" class="btn btn-default btn-sm"
                            data-dismiss="modal">{{ __('app.labels.close') }}</button>
                    <button type="submit"
                            class="btn btn-primary btn-sm">{{ __('app.labels.save') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(function () {
        const $form = $('#updateMenu')

        // J Validator
        $form.validate($validateDefaults);
        $form.ajaxForm($.extend(false, $formAjaxDefaults, {
            success: (response) => {
                processResponse(response, null, () => {
                    $modal.modal('hide')
                })
            }
        }))

        // select all by principal
        $('.select-all').on('change', (e) => {
            e.preventDefault()
            let checked = $(e.target).is(':checked')
            let id = e.target.attributes.attr_id.value
            if (checked) {
                $(`.attr_parent_id_${id}`).prop("checked", true)
            } else {
                $(`.attr_parent_id_${id}`).prop("checked", false)
            }
        })

        // select principal by select all
        $('.select-individual').on('change', (e) => {
            let id = e.target.attributes.attr_parent_id.value
            if ($(`.attr_parent_id_${id}:checked`).length == $(`.attr_parent_id_${id}`).length) {
                $(`#permission-${id}`).prop("checked", true)
            } else {
                $(`#permission-${id}`).prop("checked", false)
            }
        })

    })
</script>
@else
    @include('errors.403_modal')
@endcan
