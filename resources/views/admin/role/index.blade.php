@can('role-list')
    <!-- Navbar content -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('role.title') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('app.labels.administration') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('role.title') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- COLOR PALETTE -->
            <div class="card card-default color-palette-box">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-address-book"></i>
                        {{ __('role.title') }}
                    </h3>
                    @can('role-create')
                        <a href="{{ route('roles.create') }}"
                           class="btn btn-primary bg-gradient-primary btn-sm float-right ajaxify">
                            {{ __('role.labels.create') }}
                        </a>
                    @endcan
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="role_tb" class="table table-striped table-bordered" width="100%">
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{ __('role.labels.name') }}</th>
                            <th>{{ __('role.labels.description') }}</th>
                            <th>{{ __('role.labels.status') }}</th>
                            <th>{{ __('app.labels.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">&nbsp&nbsp</div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

    <script>
        $(function () {
            let datatable = $('#role_tb').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('roles.data') }}",
                columns: [
                    {data: 'id', name: 'id', visible: false, sortable: false, searchable: false, width: '0'},
                    {data: 'name', name: 'name', width: '20%', sortable: true, searchable: true},
                    {data: 'description', name: 'name', width: '40%', sortable: true, searchable: true},
                    {data: 'enabled', width: '20%', searchable: false, sortable: false},
                    {data: 'actions', width: '20%', sortable: false, searchable: false, class: 'text-center'}
                ]
            })

            $('#role_tb').on('click', 'input[type="checkbox"]', function (e) {
                e.preventDefault()
                let confirmMessage = $(this).is(':checked') ? '{{ __('role.messages.confirm.status_on') }}' : '{{ __('role.messages.confirm.status_off') }}'
                let element = $(this)
                confirmModal(confirmMessage, function () {
                    let id = element.closest('tr').attr('id')
                    let url = "{!! route('roles.enable_disable', ['id' => '__ID__']) !!}"
                    url = url.replace('__ID__', id)
                    pushRequest(url, null, function () {
                        datatable.draw()
                    }, 'get', null)
                })
            })
        })
    </script>
@else
    @include('errors.403')
@endcan
