@can('profile-list')
    @inject('User', '\App\Models\User')

    <style>
        .font-blue {
            color: #007bff;
        }
    </style>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-10 p-3">
                                    <h3 class="card-title">
                                        {{ __('app.labels.profile') }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">

                                    <!-- Profile Image -->
                                    <div class="card card-primary card-outline">
                                        <div class="card-body box-profile">
                                            <div class="text-center">
                                                <img class="profile-user-img img-fluid img-circle" id="preview_image"
                                                     @if(currentUser()->photo) src="{{ asset('images/images/'. currentUser()->id .'/'. $entity->photo) }}" @else src="{{ asset('images/user.png') }}" @endif
                                                     alt="User profile picture">
                                            </div>
                                            <h3 class="profile-username text-center">{{ currentUser()->fullName() }}</h3>
                                            @if(count(currentUser()->roles))
                                                <div class="text-center">
                                                    <p class="mb-2 font-blue">{{ __('user.labels.roles_assigned') }}</p>
                                                </div>
                                                <ul class="list-group list-group-unbordered mb-3">
                                                    @foreach(currentUser()->roles as $role)
                                                        <li class="list-group-item text-center">
                                                            <b>{{ $role->name }}</b>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-9">
                                    <div class="card">
                                        <div class="card-header p-2 text-center">
                                            <p class="mb-0 text-lg font-blue"><b>{{ __('user.labels.info') }}</b></p>
                                        </div><!-- /.card-header -->
                                        <div class="card-body">
                                            <div class="tab-content">
                                                <div class="active tab-pane">
                                                    <form class="form-horizontal" role="form" action="{{ route('update.profile', ['id' => $entity->id]) }}" method="post" id="updateProfile" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PUT')
                                                        <div class="form-group row">
                                                            <label for="first_name" class="col-sm-3 col-form-label">{{ __('user.labels.first_name') }}</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="{{ __('user.placeholders.first_name') }}" value="{{ $entity->first_name }}" @cannot('profile-edit') disabled @endcannot>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="last_name" class="col-sm-3 col-form-label">{{ __('user.labels.last_name') }}</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="{{ __('user.placeholders.last_name') }}" value="{{ $entity->last_name }}" @cannot('profile-edit') disabled @endcannot>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="document_type" class="col-sm-3 col-form-label">{{ __('user.labels.document_type') }}</label>
                                                            <div class="col-sm-9">
                                                                <select id="document_type" name="document_type" class="form-control select2" @cannot('profile-edit') disabled @endcannot>
                                                                    <option></option>
                                                                    @foreach($User::DOCUMENT_TYPE as $value)
                                                                        <option value="{{ $value['id'] }}" @if($value['id'] === $entity->document_type) selected @endif>
                                                                            {{ $value['description'] }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="document" class="col-sm-3 col-form-label">{{ __('user.labels.document') }}</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" name="document" id="document" placeholder="{{ __('user.placeholders.document') }}" value="{{ $entity->document }}" @cannot('profile-edit') disabled @endcannot>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="email" class="col-sm-3 col-form-label">{{ __('user.labels.email') }}</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" name="email" id="email" placeholder="{{ __('user.placeholders.email') }}" value="{{ $entity->email }}" @cannot('profile-edit') disabled @endcannot>
                                                            </div>
                                                        </div>
                                                        @can('profile-edit')
                                                            <div class="form-group row">
                                                                <label for="photo" class="col-sm-3 col-form-label">{{ __('user.labels.photo') }}</label>
                                                                <div class="col-sm-9">
                                                                    <input type="file" class="form-control" name="photo" id="photo" placeholder="{{ __('user.placeholders.photo') }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="offset-sm-3 col-sm-10">
                                                                    <button type="submit" class="btn btn-primary btn-sm">{{ __('app.labels.update') }}</button>
                                                                </div>
                                                            </div>
                                                        @endcan
                                                    </form>
                                                </div>
                                                <!-- /.tab-pane -->
                                            </div>
                                            <!-- /.tab-content -->
                                        </div><!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                <!-- /.col -->
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">&nbsp&nbsp</div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>

    <script>
        $(function () {
            const $form = $('#updateProfile')
            const documentType = $('#document_type')
            const document = $('#document')
            const email = $('#email')
            const photo = $('#photo')
            const previewImage = $('#preview_image')

            $('.select2', $form).select2({
                placeholder: "{{ trans('app.labels.select') }}",
                width: '100%',
                theme: "classic",
                allowClear: true
            }).on("select2:close", function (e) {
                $(this).valid()
            })

            photo.on('change', function () {
                readURL(this)
            })

            const readURL = (input) => {
                let filename = input.files[0].name
                let arr = filename.split(".")
                if ($(arr).get(-1) === 'jpg' || $(arr).get(-1) === 'jpeg' || $(arr).get(-1) === 'png') {
                    if (input.files && input.files[0]) {
                        let reader = new FileReader()
                        reader.onload = function (e) {
                            previewImage.attr('src', e.target.result)
                        }
                        reader.readAsDataURL(input.files[0])
                    } else {
                        alert('select a file to see preview')
                        previewImage.attr('src', '')
                    }
                }
            }

            const checkDocument = (documentTypeValue) => {
                switch (documentTypeValue) {
                    case 'CED' :
                        document.rules("remove", "passport ruc")
                        document.rules("add", {
                            cedula: true
                        })
                        break
                    case 'PAS' :
                        document.rules("remove", "cedula ruc")
                        document.rules("add", {
                            passport: true
                        })
                        break
                    case 'RUC' :
                        document.rules("remove", "cedula passport")
                        document.rules("add", {
                            ruc: true
                        })
                }
            }

            documentType.on('change', function () {
                checkDocument(this.value)
                document.valid()
            })



            $validateDefaults.rules = {
                first_name: {
                    required: true,
                    lettersOnly: true
                },
                last_name: {
                    required: true,
                    lettersOnly: true
                },
                document_type: {
                    required: true
                },
                document: {
                    required: true,
                    remote: {
                        url: "{!! route('checkuniquefield') !!}",
                        data: {
                            fieldName: 'document',
                            fieldValue: () => {
                                return $('#document', $form).val()
                            },
                            model: 'App\\Models\\User',
                            current: '{{ $entity->id }}'
                        }
                    }
                },
                email: {
                    required: true,
                    emailChecker: true,
                    remote: {
                        url: "{!! route('check_email.users') !!}",
                        data: {
                            email: function () {
                                return email.val();
                            },
                            id: '{{ $entity->id }}'
                        }
                    }
                },
                photo: {
                    extension: 'jpg|jpeg|png'
                }
            }

            $validateDefaults.messages = {
                email: {
                    remote: '{!! trans('user.messages.validation.email_exists') !!}'
                },
                document: {
                    remote: '{!! trans('user.messages.validation.document_exists') !!}'
                },
                photo: {
                    extension: '{!! trans('user.messages.validation.extension_type') !!}'
                }
            }

            // J Validator
            $form.validate($validateDefaults)
            $form.ajaxForm($formAjaxDefaults)

            checkDocument(documentType.val())
        })
    </script>
@else
    @include('errors.403')
@endcan
