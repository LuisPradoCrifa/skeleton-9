@can('profile-change_password')
    <div class="modal-header">
        <h4 class="modal-title">{{ __('app.labels.change_password') }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <form id="updateMenu" role="form" action="{{ route('update_password.profile', ['id' => $entity->id]) }}" method="post" class="form-horizontal form-label-left">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group row">
                    <label for="header" class="col-sm-2 col-form-label">{{ __('user.labels.username') }}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="{{ __('user.placeholders.username') }}" value="{{ $entity->username }}" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="slug" class="col-sm-2 col-form-label">{{ __('user.labels.password') }}</label>
                    <div class="col-sm-10">
                        <input type="password" name="password" class="form-control" id="password" placeholder="{{ __('user.placeholders.password') }}">
                    </div>
                </div>
                <div class="form-group input-group-sm row">
                    <label for="icon" class="col-sm-2 col-form-label">{{ __('user.labels.repeat_password') }}</label>
                    <div class="col-sm-10">
                        <input type="password" name="repeat_password" class="form-control" id="repeat_password" placeholder="{{ __('user.placeholders.repeat_password') }}">
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <div class="row">
                    <div class="col text-center">
                        <button type="button" class="btn btn-default btn-sm"
                                data-dismiss="modal">{{ __('app.labels.close') }}</button>
                        <button type="submit"
                                class="btn btn-primary btn-sm">{{ __('app.labels.save') }}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script>
        $(function () {
            const $form = $('#updateMenu')

            $form.validate({
                rules: {
                    password: {
                        required: true
                    },
                    repeat_password: {
                        required: true,
                        equalTo: '#password'
                    }
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback')
                    element.closest('.form-group').append(error)
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid')
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid')
                }
            })

            // J Validator
            $form.submit(function (e) {
                e.preventDefault()
                let form = $(this)
                // check if the input is valid using a 'valid' property
                if (form.valid()) {
                    $.ajax({
                        url: form[0].action,
                        type: form[0].method,
                        data: form.serialize(),
                        success: function (response) {
                            processResponse(response, null, () => {
                                $modal.modal('hide')
                            })
                        }
                    })
                }
            })
        })
    </script>
@else
    @include('errors.403_modal')
@endcan
