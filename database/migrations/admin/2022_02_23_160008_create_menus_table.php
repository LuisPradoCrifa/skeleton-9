<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('header')->default(0);
            $table->integer('parent_id')->unsigned()->nullable()->index()->foreign()->references('id')->on('menus');
            $table->string('label');
            $table->string('slug')->nullable();
            $table->string('permission')->nullable();
            $table->string('icon')->nullable();
            $table->integer('weight');
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
};
