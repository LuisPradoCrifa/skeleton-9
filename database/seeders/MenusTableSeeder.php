<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new \App\Models\admin\Menu;
        $model::truncate();

        $admin = $model->create([
            'header' => true,
            'label' => 'Administración',
            'weight' => 1000
        ]);

        $model->create([
            'label' => 'Roles',
            'slug' => 'roles.index',
            'permission' => 'role-list',
            'icon' => 'address-book',
            'weight' => 1001,
            'parent_id' => $admin->id
        ]);

        $model->create([
            'label' => 'Usuarios',
            'slug' => 'users.index',
            'permission' => 'user-list',
            'icon' => 'users',
            'weight' => 1002,
            'parent_id' => $admin->id
        ]);

        $model->create([
            'label' => 'Menus',
            'slug' => 'menus.index',
            'permission' => 'menu-list',
            'icon' => 'bars',
            'weight' => 1003,
            'parent_id' => $admin->id
        ]);
    }
}
