<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new \App\Models\User;
        $model::truncate();

        // User's
        $model->create([
            'username' => 'admin',
            'first_name' => 'admin',
            'last_name' => 'admin',
            'document_type' => 'PAS',
            'document' => '0000000000',
            'email' => 'admin@crifa.com',
            'password' => bcrypt('123456')
        ])->assignRole('admin');

        $model->create([
            'username' => 'develop',
            'first_name' => 'develop',
            'last_name' => 'develop',
            'document_type' => 'PAS',
            'document' => '0000000001',
            'email' => 'develop@crifa.com',
            'password' => bcrypt('123456')
        ])->assignRole('develop');
    }
}
