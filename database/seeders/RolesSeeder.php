<?php

namespace Database\Seeders;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create([
            'name' => 'admin',
            'description' => 'Administrador'
        ]);
        $develop = Role::create([
            'name' => 'develop',
            'description' => 'Desarrollador'
        ]);
        $user = Role::create([
            'name' => 'user',
            'description' => 'Usuario'
        ]);

        // Admin Permissions
        $arrayFind = [1, 8, 23];
        $permissionsAdmin = Permission::where(function ($query) use ($arrayFind) {
            $query->whereIn('id', $arrayFind)
                ->orWhereIn('parent_id', $arrayFind);
        })->select('id')
            ->get()
            ->toArray();
        $admin->syncPermissions($permissionsAdmin);

        // All permissions DEVELOPER
        $permissionsDeveloper = Permission::pluck('id', 'id')->all();
        $develop->syncPermissions($permissionsDeveloper);

        // User permission
        $arrayFindU = [23];
        $permissionsUser = Permission::where(function ($query) use ($arrayFindU) {
            $query->whereIn('id', $arrayFindU)
                ->orWhereIn('parent_id', $arrayFindU);
        })->select('id')
            ->get()
            ->toArray();
        $user->syncPermissions($permissionsUser);
    }
}
