<?php

namespace Database\Seeders;

use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            // User
            1 => [
                'name' => 'user',
                'parent_id' => null,
                'description' => __('user.title')
            ],
            2 => [
                'name' => 'user-list',
                'parent_id' => 1,
                'description' => __('user.labels.index')
            ],
            3 => [
                'name' => 'user-create',
                'parent_id' => 1,
                'description' => __('user.labels.create')
            ],
            4 => [
                'name' => 'user-edit',
                'parent_id' => 1,
                'description' => __('user.labels.edit')
            ],
            5 => [
                'name' => 'user-delete',
                'parent_id' => 1,
                'description' => __('user.labels.delete')
            ],
            6 => [
                'name' => 'user-show',
                'parent_id' => 1,
                'description' => __('user.labels.show')
            ],
            7 => [
                'name' => 'user-enable_disable',
                'parent_id' => 1,
                'description' => __('user.labels.enabled')
            ],
            // Role
            8 => [
                'name' => 'role',
                'parent_id' => null,
                'description' => __('role.title')
            ],
            9 => [
                'name' => 'role-list',
                'parent_id' => 8,
                'description' => __('role.labels.index')
            ],
            10 => [
                'name' => 'role-create',
                'parent_id' => 8,
                'description' => __('role.labels.create')
            ],
            11 => [
                'name' => 'role-edit',
                'parent_id' => 8,
                'description' => __('role.labels.edit')
            ],
            12 => [
                'name' => 'role-delete',
                'parent_id' => 8,
                'description' => __('role.labels.delete')
            ],
            13 => [
                'name' => 'role-show',
                'parent_id' => 8,
                'description' => __('role.labels.show')
            ],
            14 => [
                'name' => 'role-enable_disable',
                'parent_id' => 8,
                'description' => __('role.labels.enabled')
            ],
            15 => [
                'name' => 'role-permissions',
                'parent_id' => 8,
                'description' => 'Permisos rol'
            ],
            // Menu
            16 => [
                'name' => 'menu',
                'parent_id' => null,
                'description' => __('menu.title')
            ],
            17 => [
                'name' => 'menu-list',
                'parent_id' => 16,
                'description' => __('menu.labels.index')
            ],
            18 => [
                'name' => 'menu-create',
                'parent_id' => 16,
                'description' => __('menu.labels.create')
            ],
            19 => [
                'name' => 'menu-edit',
                'parent_id' => 16,
                'description' => __('menu.labels.edit')
            ],
            20 => [
                'name' => 'menu-delete',
                'parent_id' => 16,
                'description' => __('menu.labels.delete')
            ],
            21 => [
                'name' => 'menu-show',
                'parent_id' => 16,
                'description' => __('menu.labels.show')
            ],
            22 => [
                'name' => 'menu-enable_disable',
                'parent_id' => 16,
                'description' => __('menu.labels.enabled')
            ],
            // Profile
            23 => [
                'name' => 'profile',
                'parent_id' => null,
                'description' => __('app.labels.profile')
            ],
            24 => [
                'name' => 'profile-list',
                'parent_id' => 23,
                'description' => __('app.labels.show_profile')
            ],
            25 => [
                'name' => 'profile-edit',
                'parent_id' => 23,
                'description' => __('app.labels.edit_profile')
            ],
            26 => [
                'name' => 'profile-check_email',
                'parent_id' => 23,
                'description' => __('app.labels.verify_email')
            ],
            27 => [
                'name' => 'profile-change_password',
                'parent_id' => 23,
                'description' => __('app.labels.change_password')
            ],
            29 => [
                'name' => 'profile-show',
                'parent_id' => 23,
                'description' => __('app.labels.show_profile')
            ]
        ];

        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission['name'],
                'parent_id' => $permission['parent_id'],
                'description' => $permission['description']
            ]);
        }
    }
}
