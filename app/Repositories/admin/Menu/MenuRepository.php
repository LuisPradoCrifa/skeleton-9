<?php

namespace App\Repositories\admin\Menu;

use App\Models\admin\Menu;
use function is_null;

class MenuRepository implements MenuInterface
{
    public function getAllData()
    {
        return Menu::latest()->get();
    }

    public function getOnlyParents()
    {
        return Menu::whereNull('parent_id')->get();
    }

    public function findChildren($parentId)
    {
        return Menu::where('parent_id', $parentId)
            ->where('status', true)
            ->orderBy('weight', 'asc')
            ->get();
    }

    public function storeOrUpdate($id = null, $data)
    {
        if (is_null($id)) {
            $entity = new Menu();
            return $entity->create($data);
        } else {
            $entity = Menu::find($id);
            $entity->fill($data);
            $entity->save();
            return $entity->fresh();
        }
    }

    public function view($id)
    {
        return Menu::find($id);
    }

    public function changeStatus($entity)
    {
        return $entity->save();
    }

    public function destroy($id)
    {
        $entity = Menu::find($id);
        if (!$entity) {
            throw new \Exception(trans('menu.messages.exceptions.not_found'), 1000);
        }
        return Menu::find($id)->delete();
    }
}
