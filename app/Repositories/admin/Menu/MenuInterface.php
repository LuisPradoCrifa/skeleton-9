<?php

namespace App\Repositories\admin\Menu;

interface MenuInterface
{
    public function getAllData();

    public function getOnlyParents();

    public function findChildren($id);

    public function storeOrUpdate($id = null, $data);

    public function changeStatus($entity);

    public function view($id);

    public function destroy($id);
}
