<?php

namespace App\Repositories\admin\Profile;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use function is_null;
use App\Models\User;

class ProfileRepository implements ProfileInterface
{
    public function getAllData()
    {
        return User::latest()->get();
    }

    public function storeOrUpdate($id = null, $data)
    {
        if (is_null($id)) {
            $entity = new User();
            // Encrypt password
            $data['password'] = Hash::make($data['password']);
            return $entity->create($data);
        } else {
            $entity = User::find($id);
            if (isset($data['password']) && $data['password']){
                $data['password'] = Hash::make($data['password']);
            }
            if (isset($data['photo']) && $data['photo'] && $entity->photo) {
                if (File::exists(public_path('images/images/'. $id . '/' . $entity->photo))) {
                    File::delete(public_path('images/images/'. $id . '/' . $entity->photo));
                }
            }
            if (isset($data['password'])) {
                $data['password'] = Hash::make($data['password']);
            }
            $entity->fill($data);
            $entity->save();
            return $entity->fresh();
        }
    }

    public function view($id)
    {
        return User::find($id);
    }

    public function changeStatus($entity)
    {
        return $entity->save();
    }

    public function destroy($id)
    {
        $entity = User::find($id);
        if (!$entity) {
            throw new \Exception(trans('user.messages.exceptions.not_found'), 1000);
        }
        return User::find($id)->delete();
    }
}
