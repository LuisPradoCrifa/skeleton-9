<?php

namespace App\Repositories\admin\Profile;

interface ProfileInterface
{
    public function getAllData();

    public function storeOrUpdate($id = null, $data);

    public function changeStatus($entity);

    public function view($id);

    public function destroy($id);
}
