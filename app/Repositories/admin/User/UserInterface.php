<?php

namespace App\Repositories\admin\User;

interface UserInterface
{
    public function getAllData();

    public function storeOrUpdate($id = null, $data);

    public function changeStatus($entity);

    public function checkEmailExists($data);

    public function view($id);

    public function destroy($id);
}
