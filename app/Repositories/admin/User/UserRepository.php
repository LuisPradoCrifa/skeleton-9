<?php

namespace App\Repositories\admin\User;

use Illuminate\Support\Facades\Hash;
use App\Models\User;
use function is_null;

class UserRepository implements UserInterface
{
    public function getAllData()
    {
        return User::latest()->get();
    }

    public function storeOrUpdate($id = null, $data)
    {
        if (is_null($id)) {
            $entity = new User();
            // Encrypt password
            $data['password'] = Hash::make($data['password']);
            $entity->fill($data);
            $entity->save();
            // Set roles - permissions
            if (isset($data['roles'])) {
                $entity->roles()->sync($data['roles']);
            }
            return $entity;
        } else {
            $entity = User::find($id);
            $entity->fill($data);
            $entity->save();
            // Set roles - permissions
            if (isset($data['roles'])) {
                $entity->roles()->sync($data['roles']);
            }
            return $entity->fresh();
        }
    }

    public function view($id)
    {
        return User::find($id);
    }

    public function changeStatus($entity)
    {
        return $entity->save();
    }

    public function destroy($id)
    {
        $entity = User::find($id);
        if (!$entity) {
            throw new \Exception(trans('user.messages.exceptions.not_found'), 1000);
        }
        return User::find($id)->delete();
    }

    public function checkEmailExists($data)
    {
        if (isset($data['id'])) {
            $result = User::where([
                'email' => $data['email'],
                'status' => 1
            ])->where('id', '!=', $data['id'])->count();
        } else {
            $result = User::where([
                'email' => $data['email'],
                'status' => 1
            ])->count();
        }
        return $result;
    }
}
