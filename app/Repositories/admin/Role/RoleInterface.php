<?php

namespace App\Repositories\admin\Role;

interface RoleInterface
{
    public function getAllData();

    public function storeOrUpdate($id = null, $data);

    public function changeStatus($entity);

    public function view($id);

    public function destroy($id);
}
