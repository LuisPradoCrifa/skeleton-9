<?php

namespace App\Repositories\admin\Role;

use Spatie\Permission\Models\Role;
use function is_null;

class RoleRepository implements RoleInterface
{
    public function getAllData()
    {
        return Role::latest()->get();
    }

    public function storeOrUpdate($id = null, $data)
    {
        if (is_null($id)) {
            $entity = new Role();
            $data['guard_name'] = 'web';
            return $entity->create($data);
        } else {
            $entity = Role::find($id);
            $entity->fill($data);
            $entity->save();
            if (isset($data['permission']) && $data['permission']) {
                $entity->syncPermissions($data['permission']);
            }
            return $entity->fresh();
        }
    }

    public function view($id)
    {
        return Role::find($id);
    }

    public function changeStatus($entity)
    {
        return $entity->save();
    }

    public function destroy($id)
    {
        $entity = Role::find($id);
        if (!$entity) {
            throw new \Exception(trans('role.messages.exceptions.not_found'), 1000);
        }
        return Role::find($id)->delete();
    }
}
