<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class DropTablesDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sloncorp:drop {--force=no}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drops all tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $env = env('APP_ENV');
        if ($env !== 'local')
            exit(PHP_EOL . 'Drop Tables command aborted' . PHP_EOL);

        $colname = 'Tables_in_' . env('DB_DATABASE');

        $tables = DB::select('SHOW TABLES');

        if (count($tables)) {
            foreach ($tables as $table) {
                $droplist[] = $table->$colname;
            }
            $droplist = implode(',', $droplist);
            // Turn off referential integrity
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');
            DB::statement("DROP TABLE $droplist");
            // Turn referential integrity back on
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        }
        $this->comment(PHP_EOL . "If no errors showed up, all tables were dropped" . PHP_EOL);
    }
}
