<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Console\Command;

class SeedDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sloncorp:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs seeder and migrations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $env = env('APP_ENV');
        if ($env !== 'local') {
            exit(PHP_EOL . 'Drop Tables command aborted' . PHP_EOL);
        }

        if (!$this->confirm('CONFIRM DROP AL TABLES IN THE CURRENT DATABASE, MIGRATE AND SEED? [y|N]')) {
            exit(PHP_EOL . 'Command can only be executed in local environment. Check APP_ENV variable' . PHP_EOL);
        }

        Artisan::call('sloncorp:drop', array('--force' => 'yes'));
        // RUN Migrations
        Artisan::call('migrate', array('--path' => 'database/migrations/'));
        Artisan::call('migrate', array('--path' => 'database/migrations/admin'));
        Artisan::call('migrate', array('--path' => 'database/migrations/business'));
        // RUN Seeders
        Artisan::call('db:seed', array('--class' => 'DatabaseSeeder'));
    }
}
