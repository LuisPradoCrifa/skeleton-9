<?php

use Illuminate\Database\Eloquent\Collection;

/**
 * Get the current logged user
 *
 * @return \Illuminate\Contracts\Auth\Authenticatable|null
 */
function currentUser()
{
    return auth()->user();
}

/**
 * Get the permission for the role
 *
 * @param null $role
 * @return array
 */
function permissions($role = null)
{
    $result = [];

    // base permissions
    $bases = (config('acl.permission'))::whereNull('inherit_id')->where('show_to_users', '<>', '0')->get();
    foreach ($bases as $base) {
        $result[$base->id] = [
            'name' => $base->name,
            'label' => (null != $base->label ? $base->label : $base->name),
            'actions' => $base->slug
        ];
    }

    // rewrite role permissions
    if (null != $role) {
        $permissions = (config('acl.permission'))::join('permission_role', 'permission_role.permission_id', 'permissions.id')
            ->where('permission_role.role_id', $role->id)
            ->where('show_to_users', '<>', '0')
            ->get();

        foreach ($permissions as $permission) {
            if (null == $permission->inherit_id) {
                $result[$permission->permission_id] = [
                    'name' => $permission->name,
                    'label' => (null != $permission->label ? $permission->label : $permission->name),
                    'actions' => $permission->slug
                ];
            } else {
                foreach ($permission->slug as $key => $action) {
                    $result[$permission->inherit_id]['actions'][$key] = $action;
                }
            }
        }
    }

    return $result;
}

function allPermissions($role = null)
{
    $result = [];

    // base permissions
    $bases = (config('acl.permission'))::whereNull('inherit_id')->get();
    foreach ($bases as $base) {
        $result[$base->id] = [
            'name' => $base->name,
            'label' => (null != $base->label ? $base->label : $base->name),
            'actions' => $base->slug
        ];
    }

    // rewrite role permissions
    if (null != $role) {
        $permissions = (config('acl.permission'))::join('permission_role', 'permission_role.permission_id', 'permissions.id')
            ->where('permission_role.role_id', $role->id)
            ->get();

        foreach ($permissions as $permission) {
            if (null == $permission->inherit_id) {
                $result[$permission->permission_id] = [
                    'name' => $permission->name,
                    'label' => (null != $permission->label ? $permission->label : $permission->name),
                    'actions' => $permission->slug
                ];
            } else {
                foreach ($permission->slug as $key => $action) {
                    $result[$permission->inherit_id]['actions'][$key] = $action;
                }
            }
        }
    }

    return $result;
}

/**
 * Order permission.
 *
 * @param array $data
 *
 * @return Collection
 */
function order_permissions(array $data)
{
    $data_permissions = collect($data);
    $result = $data_permissions->sortBy('is_primary');
    return $result->sortBy('order');
}

/**
 * @param $date
 * @param $format
 * @return string
 */
function formatDate($date, $format)
{
    return \Carbon\Carbon::parse($date)->format($format);
}

/**
 * @param string $filename
 * @param string $delimiter
 * @return array|bool
 */
function csv_to_array($filename = '', $delimiter = ',')
{
    if (!file_exists($filename) || !is_readable($filename)) {
        return false;
    }

    $header = null;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== false) {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
            if (!array_filter($row)) {
                break;
            }

            if (!$header) {
                $header = $row;
                foreach ($header as &$h) {
                    $h = trim($h);
                }
            } else {
                $new_row = array_combine($header, $row);
                if (!is_bool($new_row)) {
                    $data[] = $new_row;
                }
            }

        }
        fclose($handle);
    }
    return $data;
}

/**
 * @param $value
 * @param $total
 * @return float|int
 */
function percent($value, $total)
{
    if ($total == 0) {
        return 0;
    }

    return round($value * 100 / $total, 2);
}

/**
 * @param Throwable $e
 * @param bool $success
 * @return mixed
 */
function defaultCatchHandler(Throwable $e, $success = true)
{
    $code = $e->getCode();
    switch ($code) {
        case 2000:
            $type = 'warning';
            break;
        default:
            $type = 'danger';
    }
    $response['message'] = [
        'title' => 'Error!',
        'type' => $type,
        'text' => $code == 1000 || $code == 2000 ? $e->getMessage() : trans('app.messages.exceptions.unexpected')
    ];
    if ($success == false) {
        $response['success'] = false;
    }
    if (!($e instanceof \Illuminate\Session\TokenMismatchException) && $code != 2000) {
        Illuminate\Support\Facades\Log::error($e->getMessage());
        Illuminate\Support\Facades\Log::error($e->getTraceAsString());
    }
    return $response;
}

/**
 * @param null $code
 * @return bool
 */
function displayException($code = null)
{
    return currentUser() && $code !== 1000 && currentUser()->hasRole(config('app.display_exception_to_roles'));
}

/**
 * @param $url
 * @return string
 */
function fullUrl($url)
{
    return url('/') . $url;
}

/**
 * @param Throwable $e
 * @return string
 */
function datatableEmptyResponse(Throwable $e)
{
    \Illuminate\Support\Facades\Log::error($e->getMessage());
    \Illuminate\Support\Facades\Log::error($e->getTraceAsString());

    return json_encode(['draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => []]);
}

/**
 * Decode string.
 *
 * @param $param
 *
 * @return mixed
 */
function decodeString($param)
{
    // cipher method
    $ciphering = env('CIPHERING_METHOD');
    $options = 0;
    // Non-NULL Initialization Vector for decryption
    $decryption_iv = env('ENCRYPTION_IV');
    $decryption_key = env('ENCRYPTION_KEY');
    $function = openssl_decrypt($param, $ciphering, $decryption_key, $options, $decryption_iv);
    return $function;
}

/**
 * Find positions in array.
 *
 * @param $array
 * @param string $field
 * @param int $search
 *
 * @return false|int|string
 */
function arraySearch($array, string $field, int $search)
{
    foreach ($array as $key => $value) {
        if (isset($value[$field]) && (int)$value[$field] === (int)$search) {
            return $key;
        }
    }
    return false;
}

/**
 * Find positions in array.
 *
 * @param $array
 * @param string $field
 * @param $search
 *
 * @return false|int|string
 */
function arraySearchString($array, string $field, $search)
{
    foreach ($array as $key => $value) {
        if (isset($value[$field]) && $value[$field] === $search) {
            return $key;
        }
    }
    return false;
}

/**
 * Return array find.
 *
 * @param $array
 * @param string $field
 * @param int $search
 *
 * @return array
 */
function arraySearchArray($array, string $field, int $search)
{
    $result = [];
    foreach ($array as $key => $value) {
        if (isset($value[$field]) && (int)$value[$field] === (int)$search) {
            array_push($result, $key);
        }
    }
    return $result;
}

/**
 * Implode code
 *
 * @param $separator
 * @param $array
 *
 * @return string
 */
function implodeCustom($separator, $array)
{
    $temp = '';
    foreach ($array as $key => $item) {
        $temp .= $separator;
        $temp .= $item . ';';
    }
    return trim($temp);
}

/**
 * Array msort.
 *
 * @param $array
 * @param $cols
 *
 * @return array
 */
function array_msort($array, $cols)
{
    $colarr = array();
    foreach ($cols as $col => $order) {
        $colarr[$col] = array();
        foreach ($array as $k => $row) {
            $colarr[$col]['_' . $k] = strtolower($row[$col]);
        }
    }
    $eval = 'array_multisort(';
    foreach ($cols as $col => $order) {
        $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
    }
    $eval = substr($eval, 0, -1) . ');';
    eval($eval);
    $ret = array();
    foreach ($colarr as $col => $arr) {
        foreach ($arr as $k => $v) {
            $k = substr($k, 1);
            if (!isset($ret[$k])) $ret[$k] = $array[$k];
            $ret[$k][$col] = $array[$k][$col];
        }
    }
    return $ret;
}
