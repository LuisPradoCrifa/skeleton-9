<?php

namespace App\Providers;

use App\Repositories\admin\Profile\ProfileRepository;
use App\Repositories\admin\Profile\ProfileInterface;
use App\Repositories\admin\User\UserRepository;
use App\Repositories\admin\Role\RoleRepository;
use App\Repositories\admin\Menu\MenuRepository;
use App\Repositories\admin\Menu\MenuInterface;
use App\Repositories\admin\Role\RoleInterface;
use App\Repositories\admin\User\UserInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Custom providers
        $this->app->bind(MenuInterface::class,MenuRepository::class);
        $this->app->bind(RoleInterface::class,RoleRepository::class);
        $this->app->bind(UserInterface::class,UserRepository::class);
        $this->app->bind(ProfileInterface::class,ProfileRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
