<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Role extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'guard_name',
        'description',
        'status'
    ];

    const ROLE_ADMIN = 1;
    const ROLE_DEVELOP = 2;
    const ROLE_USER = 3;

    /**
     * Get the post's image.
     */
    public function user()
    {
        return $this->morphOne(User::class, 'model_type');
    }
}
