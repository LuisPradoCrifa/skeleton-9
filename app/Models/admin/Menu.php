<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'menus';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'header',
        'parent_id',
        'label',
        'slug',
        'permission',
        'icon',
        'weight',
        'status'
    ];
}
