<?php

namespace App\Mail;

use Illuminate\Queue\SerializesModels;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var $username
     */
    public $username;

    /**
     * @var $token
     */
    public $token;

    /**
     * Create a new message instance.
     *
     * @param $username
     * @param $token
     */
    public function __construct($username, $token)
    {
        $this->username = $username;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user['username'] = $this->username;
        $user['token'] = $this->token;
        $user['url'] = env('APP_URL');
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('Recuperación de contraseña')
            ->view('email.reset-password', ['user' => $user]);
    }
}
