<?php

namespace App\Http\Controllers;

use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Mail\ResetPassword;
use Illuminate\Support\Str;
use App\Models\User;
use Carbon\Carbon;

class LoginController extends Controller
{

    /**
     * Validate token for forgot password
     *
     * @param string token
     * @return View
     */
    public function forgotPasswordValidate(string $token)
    {
        $user = User::where('remember_token', $token)->first();
        if ($user) {
            if (isset($user->email_verified_at)) {
                $dateTimeEmailVerified = Carbon::create($user->email_verified_at)->addMinutes(env('RECOVER_PASSWORD_TIME'));
                $currentDateTime = Carbon::now();
                if ($currentDateTime > $dateTimeEmailVerified) {
                    $message = [
                        'type' => 'danger',
                        'title' => __('app.labels.error'),
                        'message' => __('auth.time_token_expired')
                    ];
                } else {
                    $email = $user->email;
                    return view('auth.change-password', compact('email'));
                }
            }
        } else {
            $message = [
                'type' => 'danger',
                'title' => __('app.labels.error'),
                'message' => __('auth.invalid_token')
            ];
        }
        return view('auth.passwords.email', [
            'message' => $message
        ]);
    }

    /**
     * Reset password
     *
     * @param Request $request
     * @return View
     * @throws ValidationException
     */
    public function resetPassword(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            $message = [
                'type' => 'danger',
                'title' => __('app.labels.error'),
                'message' => __('auth.failed')
            ];
            return view('auth.passwords.email', [
                'message' => $message
            ]);
        } else {
            $token = Str::random(60);
            $user['remember_token'] = $token;
            $user['email_verified_at'] = now();
            $user->save();
            Mail::to($request->email)->send(new ResetPassword($user->username, $token));
            $message = [
                'type' => 'success',
                'title' => __('app.labels.done'),
                'message' => __('auth.email_sent', ['email' => $user->email])
            ];
            return view('auth.passwords.email', [
                'message' => $message
            ]);
        }
    }

    /**
     * Change password
     *
     * @param Request $request
     *
     * @return View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password'
        ],
        [
            'password.required' => 'El campo contraseña es requerido!',
            'confirm_password.required' => 'El campo confirmar contraseña es requerido!',
            'password.min' => 'La contraseña debe tener mínimo 6 caracteres!',
            'confirm_password.min' => 'La contraseña debe tener mínimo 6 caracteres!',
            'confirm_password.same' => 'Las contraseñas deben ser iguales!'
        ]);
        $user = User::where('email', $request->email)->first();
        if ($user) {
            $user['remember_token'] = null;
            $user['password'] = Hash::make($request->password);
            $user->save();
            $message = [
                'type' => 'success',
                'title' => __('app.labels.done'),
                'message' => __('auth.success')
            ];
            return view('auth.login', [
                'message' => $message
            ]);
        } else {
            $message = [
                'type' => 'danger',
                'title' => __('app.labels.error'),
                'message' => __('auth.failed')
            ];
            return view('auth.passwords.email', [
                'message' => $message
            ]);
        }
    }
}
