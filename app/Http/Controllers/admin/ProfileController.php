<?php

namespace App\Http\Controllers\admin;

use App\Repositories\admin\Profile\ProfileInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use Throwable;

class ProfileController extends Controller
{
    protected $profile;

    public function __construct(ProfileInterface $profile)
    {
        $this->profile = $profile;
        // Set permissions
        $this->middleware('permission:profile-list|profile-create|profile-edit|profile-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:profile-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:profile-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:profile-delete', ['only' => ['destroy']]);
        $this->middleware('permission:profile-show', ['only' => ['show']]);
        $this->middleware('permission:profile-delete', ['only' => ['delete']]);
    }

    /**
     * Index.
     *
     * @return JsonResponse
     */
    public function index()
    {
        try {
            $entity = currentUser();
            $response['view'] = view('admin.profile.index', [
                'entity' => $entity
            ])->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * changePassword.
     *
     * @return JsonResponse
     */
    public function changePassword()
    {
        try {
            $entity = currentUser();
            $response['modal'] = view('admin.profile.change_password', [
                'entity' => $entity
            ])->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * updatePassword.
     *
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function updatePassword(int $id, Request $request)
    {
        try {
            $data = $request->only([
                'password'
            ]);
            $this->profile->storeOrUpdate($id, $data);
            $response = [
                'message' => [
                    'title' => __('app.labels.done'),
                    'type' => 'success',
                    'text' => __('user.messages.success.updated')
                ]
            ];
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * update.
     *
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update(int $id, Request $request)
    {
        try {
            $data = $request->only([
                'first_name',
                'last_name',
                'document_type',
                'document',
                'email',
                'username',
                'password',
                'photo'
            ]);
            // Verify photo
            if ($request->file('photo')) {
                $data['photo'] = self::createDataUpdate($request, $id);
            } else {
                unset($data['photo']);
            }
            $this->profile->storeOrUpdate($id, $data);
            $user = currentUser();
            $entity = User::find($user->id);
            $response = [
                'view' => view('admin.profile.index', [
                    'entity' => $entity
                ])->render(),
                'message' => [
                    'title' => __('app.labels.done'),
                    'type' => 'success',
                    'text' => __('user.messages.success.updated')
                ]
            ];
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Create data update structure.
     *
     * @param Request $request
     * @param int $id
     *
     * @return array
     */
    private function createDataUpdate(Request $request, int $id)
    {
        $file = $request->photo;
        $fileName = $file->getClientOriginalName();
        $path = env('FILES_PATH') . $id;
        $file->move($path, $fileName);
        return $fileName;
    }
}
