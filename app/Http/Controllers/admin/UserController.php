<?php

namespace App\Http\Controllers\admin;

use App\Repositories\admin\User\UserInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Models\admin\Role;
use App\Models\User;
use Exception;
use Throwable;

class UserController extends Controller
{
    protected $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
        // Set permissions
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
        $this->middleware('permission:user-show', ['only' => ['show']]);
        $this->middleware('permission:user-delete', ['only' => ['delete']]);
    }

    /**
     * Index.
     *
     * @return JsonResponse
     */
    public function index()
    {
        try {
            $response['view'] = view('admin.user.index', [
                'users' => $this->user->getAllData()
            ])->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Data.
     *
     * @param Request $request
     *
     * @return void
     * @throws \Exception
     */
    public function data(Request $request)
    {
        try {
            if ($request->ajax()) {
                $user = currentUser();
                $data = User::all()->sortBy('weight');
                $actions = [];
                if ($user->can('user-edit')) {
                    $actions['edit'] = [
                        'route' => 'edit.users',
                        'tooltip' => __('user.labels.edit')
                    ];
                }
                if ($user->can('user-show')) {
                    $actions['eye'] = [
                        'route' => 'show.users',
                        'tooltip' => __('user.labels.show')
                    ];
                }
                if ($user->can('user-delete')) {
                    $actions['trash'] = [
                        'route' => 'destroy.users',
                        'tooltip' => __('app.labels.delete'),
                        'confirm_message' => __('user.messages.confirm.delete'),
                        'method' => 'delete',
                        'btn_class' => 'btn-danger'
                    ];
                }
                $disable = true;
                if ($user->can('user-enable_disable')) {
                    $disable = false;
                }
                return Datatables::of($data)
                    ->setRowId('id')
                    ->addColumn('actions', function ($entity) use ($actions) {
                        return view('layout.partial.datatable_action', [
                            'entity' => $entity,
                            'actions' => $actions
                        ]);
                    })
                    ->addColumn('full_name', function ($entity) use ($actions) {
                        return $entity->fullName();
                    })
                    ->addColumn('enabled', function ($entity) use ($user, $disable) {
                        $checked = $entity->status ? 'checked' : '';
                        $is_disable = $disable ? 'disabled' : '';
                        return "<div class='form-group text-center'>
                                    <div class='custom-control custom-switch'>
                                        <input type='checkbox' class='custom-control-input' id='customSwitch" . $entity->id . "' " . $checked . " " . $is_disable . ">
                                        <label class='custom-control-label' for='customSwitch" . $entity->id . "'></label>
                                    </div>
                                </div>";
                    })
                    ->editColumn('icon', function ($entity) {
                        $icon = $entity->icon;
                        return "<i class='fa fa-{$icon}' aria-hidden='true'></i>";
                    })
                    ->rawColumns(['actions', 'icon', 'enabled'])
                    ->make(true);
            }
        } catch (Throwable $e) {
            return datatableEmptyResponse($e);
        }
    }

    /**
     * Create.
     *
     * @return JsonResponse
     */
    public function create()
    {
        try {
            $response['modal'] = view('admin.user.create',[
                'roles' => Role::where('status', 1)->get()
            ])->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Store.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $data = $request->only([
                'first_name',
                'last_name',
                'document_type',
                'document',
                'email',
                'username',
                'password',
                'roles',
                'photo'
            ]);
            $this->user->storeOrUpdate($id = null, $data);
            $response = [
                'view' => view('admin.user.index')->render(),
                'message' => [
                    'title' => __('app.labels.done'),
                    'type' => 'success',
                    'text' => __('user.messages.success.created')
                ]
            ];
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Edit
     * @param int $id
     * @return JsonResponse
     */
    public function edit(int $id)
    {
        try {
            $entity = User::find($id);
            $user_roles = count($entity->roles) ? $entity->roles->pluck('id')->toArray() : [];
            $response['modal'] = view('admin.user.update', [
                'entity' => $entity,
                'roles' => Role::where('status', 1)->get(),
                'user_roles' => $user_roles
            ])->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update(int $id, Request $request)
    {
        try {
            $data = $request->only([
                'first_name',
                'last_name',
                'document_type',
                'document',
                'email',
                'username',
                'password',
                'roles',
                'photo'
            ]);
            $this->user->storeOrUpdate($id, $data);
            $response = [
                'view' => view('admin.user.index')->render(),
                'message' => [
                    'title' => __('app.labels.done'),
                    'type' => 'success',
                    'text' => __('user.messages.success.updated')
                ]
            ];
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Show
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        try {
            $response['modal'] = view('admin.user.show', [
                'entity' => User::find($id),
                'roles' => Role::where('status', 1)->get()
            ])->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Enable or Disable this model.
     *
     * @param $id
     *
     * @return JsonResponse
     */
    public function enableDisable($id)
    {
        try {
            $entity = User::find($id);
            if (!$entity) {
                throw new Exception(__('user.messages.exceptions.not_found'), 1000);
            }
            $entity->status = !$entity->status;
            if (!$this->user->changeStatus($entity)) {
                throw new Exception(__('user.messages.errors.update'), 1000);
            }
            $response['message'] = [
                'title' => __('app.labels.done'),
                'type' => 'success',
                'text' => __('user.messages.success.updated')
            ];
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Destroy.
     *
     * @param $id
     *
     * @return JsonResponse
     */
    public function destroy($id)
    {
        try {
            $response = $this->user->destroy($id);
            if ($response) {
                $response = [
                    'view' => view('admin.user.index')->render(),
                    'message' => [
                        'title' => __('app.labels.done'),
                        'type' => 'success',
                        'text' => __('user.messages.success.deleted')
                    ]
                ];
            } else {
                $response = [
                    'view' => view('admin.user.index')->render(),
                    'message' => [
                        'title' => __('app.labels.error'),
                        'type' => 'alert',
                        'text' => __('user.messages.alert.delete')
                    ]
                ];
            }
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Check Email Exists.
     *
     * @param Request $request
     *
     * @return string
     */
    public function checkEmailExists(Request $request)
    {
        $result = $this->user->checkEmailExists($request->all());
        return json_encode(!$result);
    }
}
