<?php

namespace App\Http\Controllers\admin;

use App\Repositories\admin\Menu\MenuInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Models\admin\Menu;
use Exception;
use Throwable;

class MenuController extends Controller
{
    protected $menu;

    public function __construct(
        MenuInterface $menu
    )
    {
        $this->menu = $menu;
        // Set permissions
        $this->middleware('permission:menu-list|menu-create|menu-edit|menu-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:menu-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:menu-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:menu-delete', ['only' => ['destroy']]);
        $this->middleware('permission:menu-show', ['only' => ['show']]);
        $this->middleware('permission:menu-delete', ['only' => ['delete']]);
    }

    /**
     * Index.
     *
     * @return JsonResponse
     */
    public function index()
    {
        try {
            $response['view'] = view('admin.menu.index', [
                'menus' => $this->menu->getAllData()
            ])->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Data.
     *
     * @param Request $request
     *
     * @return void
     * @throws Exception
     */
    public function data(Request $request)
    {
        try {
            if ($request->ajax()) {
                $user = currentUser();
                $data = Menu::all()->sortBy('weight');
                $actions = [];
                if ($user->can('menu-edit')) {
                    $actions['edit'] = [
                        'route' => 'edit.menus',
                        'tooltip' => __('menu.labels.edit')
                    ];
                }
                if ($user->can('menu-show')) {
                    $actions['eye'] = [
                        'route' => 'show.menus',
                        'tooltip' => __('menu.labels.show')
                    ];
                }
                if ($user->can('menu-delete')) {
                    $actions['trash'] = [
                        'route' => 'destroy.menus',
                        'tooltip' => __('app.labels.delete'),
                        'confirm_message' => __('menu.messages.confirm.delete'),
                        'method' => 'delete',
                        'btn_class' => 'btn-danger'
                    ];
                }
                $disable = true;
                if ($user->can('menu-enable_disable')) {
                    $disable = false;
                }
                return Datatables::of($data)
                    ->setRowId('id')
                    ->addColumn('actions', function ($entity) use ($actions) {
                        return view('layout.partial.datatable_action', [
                            'entity' => $entity,
                            'actions' => $actions
                        ]);
                    })
                    ->addColumn('enabled', function ($entity) use ($user, $disable) {
                        $checked = $entity->status ? 'checked' : '';
                        $is_disable = $disable ? 'disabled' : '';
                        return "<div class='form-group text-center'>
                                    <div class='custom-control custom-switch'>
                                        <input type='checkbox' class='custom-control-input' id='customSwitch" . $entity->id . "' " . $checked . " " . $is_disable . ">
                                        <label class='custom-control-label' for='customSwitch" . $entity->id . "'></label>
                                    </div>
                                </div>";
                    })
                    ->editColumn('icon', function ($entity) {
                        $icon = $entity->icon;
                        return "<i class='fa fa-{$icon}' aria-hidden='true'></i>";
                    })
                    ->rawColumns(['actions', 'icon', 'enabled'])
                    ->make(true);
            }
        } catch (Throwable $e) {
            return datatableEmptyResponse($e);
        }
    }

    /**
     * Create.
     *
     * @return JsonResponse
     */
    public function create()
    {
        try {
            $response['modal'] = view('admin.menu.create')->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Store.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $data = $request->only([
                'header',
                'label',
                'slug',
                'icon',
                'weight'
            ]);
            $this->menu->storeOrUpdate($id = null, $data);
            $response = [
                'view' => view('admin.menu.index', [
                    'reload' => true
                ])->render(),
                'message' => [
                    'title' => __('app.labels.done'),
                    'type' => 'success',
                    'text' => __('menu.messages.success.created')
                ]
            ];
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Edit
     *
     * @param int $id
     * @return JsonResponse
     */
    public function edit(int $id)
    {
        try {
            $entity = Menu::find($id);
            $response['modal'] = view('admin.menu.update', [
                'entity' => $entity
            ])->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update(int $id, Request $request)
    {
        try {
            $data = $request->only([
                'header',
                'label',
                'slug',
                'icon',
                'weight'
            ]);
            $this->menu->storeOrUpdate($id, $data);
            $response = [
                'view' => view('admin.menu.index', [
                    'reload' => true
                ])->render(),
                'message' => [
                    'title' => __('app.labels.done'),
                    'type' => 'success',
                    'text' => __('menu.messages.success.updated')
                ]
            ];
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Show
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        try {
            $entity = Menu::find($id);
            $response['modal'] = view('admin.menu.show', [
                'entity' => $entity
            ])->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Enable or Disable this model.
     *
     * @param $id
     *
     * @return JsonResponse
     */
    public function enableDisable($id)
    {
        try {
            $entity = Menu::find($id);
            if (!$entity) {
                throw new Exception(__('menu.messages.exceptions.not_found'), 1000);
            }
            $entity->status = !$entity->status;
            if (!$this->menu->changeStatus($entity)) {
                throw new Exception(__('menu.messages.errors.update'), 1000);
            }
            $response = [
                'view' => view('admin.menu.index', [
                    'reload' => true
                ])->render(),
                'message' => [
                    'title' => __('app.labels.done'),
                    'type' => 'success',
                    'text' => __('menu.messages.success.updated')
                ]
            ];
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Destroy.
     *
     * @param $id
     *
     * @return JsonResponse
     */
    public function destroy($id)
    {
        try {
            $response = $this->menu->destroy($id);
            if ($response) {
                $response = [
                    'view' => view('admin.menu.index', [
                        'reload' => true
                    ])->render(),
                    'message' => [
                        'title' => __('app.labels.done'),
                        'type' => 'success',
                        'text' => __('menu.messages.success.deleted')
                    ]
                ];
            } else {
                $response = [
                    'view' => view('admin.menu.index')->render(),
                    'message' => [
                        'title' => __('app.labels.error'),
                        'type' => 'alert',
                        'text' => __('menu.messages.alert.delete')
                    ]
                ];
            }
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }
}
