<?php

namespace App\Http\Controllers\admin;

use App\Repositories\admin\Role\RoleInterface;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Exception;
use Throwable;

class RoleController extends Controller
{
    protected $role;

    public function __construct(RoleInterface $role)
    {
        $this->role = $role;
        // Set permissions
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
        $this->middleware('permission:role-show', ['only' => ['show']]);
        $this->middleware('permission:role-delete', ['only' => ['delete']]);
        $this->middleware('permission:role-permissions', ['only' => ['permissions']]);
    }

    /**
     * Index.
     *
     * @return JsonResponse
     */
    public function index()
    {
        try {
            $response['view'] = view('admin.role.index', [
                'roles' => $this->role->getAllData()
            ])->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Data.
     *
     * @param Request $request
     *
     * @return void
     */
    public function data(Request $request)
    {
        try {
            if ($request->ajax()) {
                $user = currentUser();
                $actions = [];
                if ($user->can('role-edit')) {
                    $actions['edit'] = [
                        'route' => 'edit.roles',
                        'tooltip' => __('role.labels.edit')
                    ];
                }
                if ($user->can('role-show')) {
                    $actions['eye'] = [
                        'route' => 'show.roles',
                        'tooltip' => __('role.labels.show')
                    ];
                }
                if ($user->can('role-delete')) {
                    $actions['trash'] = [
                        'route' => 'destroy.roles',
                        'tooltip' => __('app.labels.delete'),
                        'confirm_message' => __('role.messages.confirm.delete'),
                        'method' => 'delete',
                        'btn_class' => 'btn-danger'
                    ];
                }
                $disable = true;
                if ($user->can('role-enable_disable')) {
                    $disable = false;
                }
                if ($user->can('role-permissions')) {
                    $actions['list'] = [
                        'route' => 'roles.permissions',
                        'tooltip' => __('role.labels.permissions')
                    ];
                }
                $data = Role::all()->sortBy('weight');
                return Datatables::of($data)
                    ->setRowId('id')
                    ->addColumn('actions', function ($entity) use ($actions) {
                        return view('layout.partial.datatable_action', [
                            'entity' => $entity,
                            'actions' => $actions
                        ]);
                    })
                    ->addColumn('enabled', function ($entity) use ($user, $disable) {
                        $checked = $entity->status ? 'checked' : '';
                        $is_disable = $disable ? 'disabled' : '';
                        return "<div class='form-group text-center'>
                                    <div class='custom-control custom-switch'>
                                        <input type='checkbox' class='custom-control-input datatable' id='customSwitch" . $entity->id . "' " . $checked . " " . $is_disable . ">
                                        <label class='custom-control-label' for='customSwitch" . $entity->id . "'></label>
                                    </div>
                                </div>";
                    })
                    ->editColumn('icon', function ($entity) {
                        $icon = $entity->icon;
                        return "<i class='fa fa-{$icon}' aria-hidden='true'></i>";
                    })
                    ->rawColumns(['actions', 'icon', 'enabled'])
                    ->make(true);
            }
        } catch (Throwable $e) {
            return datatableEmptyResponse($e);
        }
    }

    /**
     * Create.
     *
     * @return JsonResponse
     */
    public function create()
    {
        try {
            $response['modal'] = view('admin.role.create')->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Store.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $data = $request->only([
                'name',
                'description'
            ]);
            $this->role->storeOrUpdate($id = null, $data);
            $response = [
                'view' => view('admin.role.index')->render(),
                'message' => [
                    'title' => __('app.labels.done'),
                    'type' => 'success',
                    'text' => __('role.messages.success.created')
                ]
            ];
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Edit
     *
     * @param int $id
     * @return JsonResponse
     */
    public function edit(int $id)
    {
        try {
            $entity = Role::find($id);
            $response['modal'] = view('admin.role.update', [
                'entity' => $entity
            ])->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update(int $id, Request $request)
    {
        try {
            $data = $request->only([
                'name',
                'permission',
                'description'
            ]);
            $this->role->storeOrUpdate($id, $data);
            $response = [
                'view' => view('admin.role.index')->render(),
                'message' => [
                    'title' => __('app.labels.done'),
                    'type' => 'success',
                    'text' => __('role.messages.success.updated')
                ]
            ];
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Show
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        try {
            $entity = Role::find($id);
            $response['modal'] = view('admin.role.show', [
                'entity' => $entity
            ])->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Enable or Disable this model.
     *
     * @param $id
     *
     * @return JsonResponse
     */
    public function enableDisable($id)
    {
        try {
            $entity = Role::find($id);
            if (!$entity) {
                throw new Exception(__('role.messages.exceptions.not_found'), 1000);
            }
            if (count($entity->users)) {
                $response['message'] = [
                    'title' => __('app.labels.warning'),
                    'type' => 'warning',
                    'text' => __('role.messages.validation.has_users')
                ];
            } else {
                $entity->status = !$entity->status;
                if (!$this->role->changeStatus($entity)) {
                    throw new Exception(__('role.messages.errors.update'), 1000);
                }
                $response['message'] = [
                    'title' => __('app.labels.done'),
                    'type' => 'success',
                    'text' => __('role.messages.success.updated')
                ];
            }
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Destroy.
     *
     * @param $id
     *
     * @return JsonResponse
     */
    public function destroy($id)
    {
        try {
            $entity = Role::find($id);
            if (!$entity) {
                throw new Exception(__('role.messages.exceptions.not_found'), 1000);
            }
            if (count($entity->users)) {
                $response = [
                    'view' => view('admin.role.index')->render(),
                    'message' => [
                        'title' => __('app.labels.warning'),
                        'type' => 'warning',
                        'text' => __('role.messages.validation.has_users')
                    ]
                ];
            } else {
                $response = $this->role->destroy($id);
                if ($response) {
                    $response = [
                        'view' => view('admin.role.index')->render(),
                        'message' => [
                            'title' => __('app.labels.done'),
                            'type' => 'success',
                            'text' => __('role.messages.success.deleted')
                        ]
                    ];
                } else {
                    $response = [
                        'view' => view('admin.role.index')->render(),
                        'message' => [
                            'title' => __('app.labels.error'),
                            'type' => 'alert',
                            'text' => __('role.messages.alert.delete')
                        ]
                    ];
                }
            }
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    /**
     * Permissions.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function permissions(int $id)
    {
        try {
            $entity = Role::find($id);
            $permission = Permission::get();
            $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
                ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
                ->all();
            $response['modal'] = view('admin.role.permissions', [
                'entity' => $entity,
                'permission' => $permission,
                'rolePermissions' => $rolePermissions
            ])->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }
}
