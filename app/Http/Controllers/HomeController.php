<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repositories\admin\Menu\MenuInterface;
use Illuminate\Contracts\Support\Renderable;
use Throwable;

class HomeController extends Controller
{
    protected $menu;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        MenuInterface $menu
    )
    {
        $this->menu = $menu;
        $this->middleware('auth');
    }

    /**
     * Show the application home template.
     *
     * @return Renderable
     */
    public function index()
    {
        $user = currentUser();
        $APP_NAME = env('_APP_NAME');
        $ICON = env('ICON');
        $CREATOR_URL = env('CREATOR_URL');
        $CREATOR_NAME = env('CREATOR_NAME');
        $menu = self::compose($this->menu->getOnlyParents());
        $response1 = null;
        $response2 = null;
        // Verify if has role's
        if (!count($user->roles)) {
            $response1 = __('app.labels.no_roles');
        }
        // Verify if has menu's
        if (!count($menu)) {
            $response2 = __('app.labels.no_menu');
        }
        return view('template/home', compact('APP_NAME', 'ICON', 'CREATOR_URL', 'CREATOR_NAME', 'menu', 'response1', 'response2'));
    }

    /**
     * Render app dashboard
     *
     * @return JsonResponse
     */
    public function dashboard()
    {
        try {
            $response['view'] = view('dashboard')->render();
        } catch (Throwable $e) {
            $response = defaultCatchHandler($e);
        }
        return response()->json($response);
    }

    // Find menu by role

    /**
     * _fillMenu.
     *
     * @param $menu
     *
     * @return array
     */
    private function _fillMenu($menu)
    {
        $result = [];
        $children = $this->menu->findChildren($menu->id);
        if (count($children) == 0) {
            if (null != $menu->slug && currentUser()->can($menu->permission) && $this->checkRole($menu)) {
                $result[$menu->id] = [
                    'label' => $menu->label,
                    'slug' => $menu->slug
                ];
                if (null != $menu->icon) {
                    $result[$menu->id]['icon'] = $menu->icon;
                }
            }
        } else {
            $resultChildren = [];
            foreach ($children as $child) {
                $resultChildren += $this->_fillMenu($child);
            }
            if (count($resultChildren) > 0) {
                $result[$menu->id] = [
                    'label' => $menu->label,
                    'children' => $resultChildren
                ];
                if (null != $menu->icon) {
                    $result[$menu->id]['icon'] = $menu->icon;
                }
            }
        }
        return $result;
    }

    /**
     * @param $menu
     * @return bool
     */
    function checkRole($menu)
    {

        if (isset($menu->role)) {
            $roles = explode("|", $menu->role);
            foreach ($roles as $index => $role) {
                switch ($role) {
                    case 'default':
                        return true;
                        break;
                }
            }
            return false;
        } else
            return true;
    }

    /**
     * Bind data to the view.
     *
     * @return array
     */
    public function compose($menus)
    {
        $result = [];
        foreach ($menus as $parent) {
            $result += $this->_fillMenu($parent);
        }
        return $result;
    }
}
