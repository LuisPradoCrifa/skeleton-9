# BACKOFFICE SKELETON

## Instalación:

* Requiere PHP 8.1.*, MySQL 8.*

### Para una instalación fresca:
    
1. Instalar dependencias.
    * `composer install --prefer-dist`

2. Configurar parámetros.
    * Copiar `.env.example` a `.env` y actualizar los parámetros por defecto con los reales.
    * Crear el esquemas de base de datos.

3. Generar token de seguridad.
    * `php artisan key:generate`

4. Instalar plugins de presentación.
    * `npm install --save`

5. Generar los assets.
    * `npm run dev`

6. Publicar configuraciones de vendors.
    * `php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"`

7. Crear tablas de base de datos e insertar datos base
    * En ambiente de desarrollo se puede usar el comando `php artisan sloncorp:seed`. Este comando borra todas las tablas y vuelve a crear tablas y datos base.

8. Asegurarse que las librerías `php-mysqlnd`, `php-pdo`, `php71-php-zip`

9. Crear directorio para almacenar imágenes (ejemplo en la máquina virtual Vagrant).
    * `# sudo mkdir -p /var/www/images/skeleton-9`
    * `# sudo chmod -R 777 /var/www/images/skeleton-9`
    * `# sudo ln -s /var/www/images/skeleton-9 /home/vagrant/code/skeleton-9/public/images/images`
    
10. Asegurarse que cuando se encienda el servidor los servicios se inicien: `httpd`, `mysql`
11. Agregar corrector ortográfico (Only developers):
    `https://www.jesusamieiro.com/como-anadir-un-corrector-ortografico-de-espanol-a-phpstorm/`

