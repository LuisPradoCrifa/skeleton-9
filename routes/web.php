<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('dashboard.app');

Route::get('forgot-password-verify/{token}', [App\Http\Controllers\LoginController::class, 'forgotPasswordValidate'])->name('forgot-password-verify');
Route::post('forgot-password', [App\Http\Controllers\LoginController::class, 'resetPassword'])->name('password-send-email');
Route::put('reset-password', [App\Http\Controllers\LoginController::class, 'updatePassword'])->name('reset-password');

/* ---------------- */
/* Global Functions */
/* ---------------- */
Route::group([
    'middleware' => ['auth']
],
    function () {
        Route::get('configuration/checkuniquefield', [App\Http\Controllers\System\UtilsController::class, 'checkUniqueField'])->name('checkuniquefield');
    }
);

/* ---------------- */
/* Routes admin */
/* ---------------- */
Route::group(['middleware' => ['auth']], function() {
    // Profile
    Route::get('profile/index', [App\Http\Controllers\admin\ProfileController::class, 'index'])->name('index.profile');
    Route::get('profile/edit', [App\Http\Controllers\admin\ProfileController::class, 'edit'])->name('edit.profile');
    Route::put('profile/update/{id}', [App\Http\Controllers\admin\ProfileController::class, 'update'])->name('update.profile');
    Route::put('profile/check/email', [App\Http\Controllers\admin\ProfileController::class, 'checkEmail'])->name('email_verify.profile');
    Route::get('profile/password', [App\Http\Controllers\admin\ProfileController::class, 'changePassword'])->name('change_password.profile');
    Route::put('profile/password/{id}', [App\Http\Controllers\admin\ProfileController::class, 'updatePassword'])->name('update_password.profile');

    // Menu
    Route::get('menus/data', [App\Http\Controllers\admin\MenuController::class, 'data'])->name('menus.data');
    Route::get('menus/enable_disable/{id}', [App\Http\Controllers\admin\MenuController::class, 'enableDisable'])->name('menus.enable_disable');
    Route::resource('menus', App\Http\Controllers\admin\MenuController::class, [
        'parameters' => ['menus' => 'id'],
        'names' => [
            'show' => 'show.menus',
            'store' => 'store.menus',
            'edit' => 'edit.menus',
            'update' => 'update.menus',
            'destroy' => 'destroy.menus'
        ]
    ]);

    // Roles
    Route::get('roles/data', [App\Http\Controllers\admin\RoleController::class, 'data'])->name('roles.data');
    Route::get('roles/enable_disable/{id}', [App\Http\Controllers\admin\RoleController::class, 'enableDisable'])->name('roles.enable_disable');
    Route::get('roles/permissions/{id}', [App\Http\Controllers\admin\RoleController::class, 'permissions'])->name('roles.permissions');
    Route::resource('roles', App\Http\Controllers\admin\RoleController::class, [
        'parameters' => ['roles' => 'id'],
        'names' => [
            'show' => 'show.roles',
            'store' => 'store.roles',
            'edit' => 'edit.roles',
            'update' => 'update.roles',
            'destroy' => 'destroy.roles'
        ]
    ]);

    // Usuarios
    Route::get('users/check_email', [App\Http\Controllers\admin\UserController::class, 'checkEmailExists'])->name('check_email.users');
    Route::get('users/data', [App\Http\Controllers\admin\UserController::class, 'data'])->name('users.data');
    Route::get('users/enable_disable/{id}', [App\Http\Controllers\admin\UserController::class, 'enableDisable'])->name('users.enable_disable');
    Route::resource('users', App\Http\Controllers\admin\UserController::class, [
        'parameters' => ['users' => 'id'],
        'names' => [
            'show' => 'show.users',
            'store' => 'store.users',
            'edit' => 'edit.users',
            'update' => 'update.users',
            'destroy' => 'destroy.users'
        ]
    ]);
});
