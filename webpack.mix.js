const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();

mix.copy('resources/backend', 'public/backend');
mix.copy('resources/js/main.js', 'public/backend/js/main.js');
mix.copy('resources/js/font-awesome', 'public/js/font-awesome');
mix.copy('node_modules/bootbox', 'public/js/bootbox');
mix.copy('node_modules/jquery-form', 'public/js/jquery-form');
mix.copy('node_modules/datatables-bootstrap', 'public/js/datatables-bootstrap');
mix.copy('resources/images', 'public/images');
mix.copy('bower_components/select2', 'public/js/select2');
